import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Param, UseInterceptors } from '@nestjs/common'
import { CategoryService } from './category.service'

@UseInterceptors(CacheInterceptor)
@Controller('landing/categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Get('find')
  findMany() {
    return this.categoryService.findMany()
  }

  @Get(':slug')
  getCategoryBySlug(@Param('slug') slug: string) {
    return this.categoryService.findCategoryBySlug(slug)
  }
}
