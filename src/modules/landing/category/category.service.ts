import { BaseService } from '@/modules/base/service'
import { Injectable, NotFoundException } from '@nestjs/common'

@Injectable()
export class CategoryService extends BaseService {
  async findMany() {
    return `This action find all categories`
  }

  findOne(id: number) {
    return `This action returns a #${id} category`
  }

  async findCategoryBySlug(slug: string) {
    const collection = this._prisma.category
    const parentMenu = await collection.findFirst({
      where: { slug: slug, isActive: true },
      select: {
        id: true,
        parentId: true,
        name: true,
        slug: true,
        banner: true,
        description: true
      }
    })

    if (!parentMenu) throw new NotFoundException()

    const { id, parentId, ...output } = parentMenu

    const subMenu = await collection.findMany({
      where: { AND: [{ parentId: id }, { parentId: { not: null } }], isActive: true },
      select: { name: true, slug: true }
    })

    if (parentId) {
      const sameMenu = await collection.findMany({
        where: { parentId: parentId },
        select: { name: true, slug: true }
      })
      output['sameMenu'] = sameMenu
    }
    output['subMenu'] = subMenu

    return output
  }
}
