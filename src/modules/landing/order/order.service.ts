import { IListQuery } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { strToPhone, toCurrency, toId } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { CreateOrderDto } from './order.dto'

@Injectable()
export class OrderService extends BaseService {
  async getOrders(query: IListQuery, userId: string) {
    const { skip, take } = this.withLimitOffset(query)
    const where: Prisma.OrderWhereInput = { userId }
    if (query.status) {
      where.status = query.status
    }
    const [data, totalRecords] = await Promise.all([
      this._prisma.order.findMany({
        select: {
          code: true,
          createdAt: true,
          totalAmount: true,
          totalProduct: true,
          status: true,
          paymentStatus: true,
          deliveryStatus: true,
          customerAddress: true,
          note: true,
          ordersProductsLinks: {
            select: {
              currency: true,
              price: true,
              amount: true,
              saleOff: true,
              product: { select: { slug: true, id: true, name: true, images: true } },
              options: true
            }
          }
        },
        orderBy: { createdAt: 'desc' },
        where,
        skip,
        take
      }),
      this._prisma.order.count({ where })
    ])
    data.forEach((o) => {
      o.totalAmount = toCurrency(o.totalAmount) as any
      o.ordersProductsLinks.forEach((op) => {
        const totalQuantity = op.options.reduce((sum, option: any) => sum + option?.quantity, 0)
        op.amount = toCurrency(op.amount) as any
        op.price = toCurrency(Number(op.price) * totalQuantity || 0) as any
      })
    })

    return { data, pagination: { page: query.page, pageSize: query.pageSize, totalRecords } }
  }

  async create(input: CreateOrderDto, ip: string, user: { id: string; name: string }) {
    const { products } = input
    // validate products
    const { valid, items } = await this.validateProducts(products)
    if (!valid) {
      return this.withBadRequest('Some product invalid.')
    }

    let totalAmount = 0
    const ordersProductsLinks: Prisma.OrderProductLinkCreateManyInput[] = []
    products.forEach((p) => {
      const item = items?.find((i) => i.slug === p.slug)
      if (item && item.productsCurrencies) {
        const { currency, price, saleOff } = item.productsCurrencies[0]
        const amount =
          Number(this.calculateSaleOffPrice(price || 0, saleOff || 0).toFixed(0)) *
          p.options.reduce((p, c) => p + c.quantity, 0)

        ordersProductsLinks.push({
          id: toId(),
          amount: Number(amount),
          price: price,
          productId: item.id,
          saleOff,
          currency,
          options: p.options
        })
        totalAmount += Number(amount)
      }
    })
    const now = new Date()

    return this._prisma.$transaction(
      async (tx) => {
        // create user address
        await this.createUserAddress(tx.userAddress, input, user.id)

        return tx.order.create({
          data: {
            id: toId(),
            code: this.generateCode(),
            createdAt: now,
            histories: {
              create: { id: toId(), createdAt: now, action: [{ content: 'Order created', createdAt: now }] }
            },
            totalAmount: totalAmount,
            totalProduct: products.length,
            userId: user.id,
            ordersProductsLinks: { createMany: { data: ordersProductsLinks } },
            ip: input.ip || ip,
            customerAddress: input.address,
            customerName: user.name,
            customerPhone: strToPhone(input.phone),
            country: input.country
          },
          select: { createdAt: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  private generateCode() {
    return 'FLX-' + Math.floor(1000000000 + Math.random() * 9000000000)
  }

  private async createUserAddress(userAddressRepo: Prisma.UserAddressDelegate, input: CreateOrderDto, userId: string) {
    const hasAddress = await userAddressRepo.count({ where: { userId } })
    if (hasAddress === 0) {
      await userAddressRepo.create({
        select: { id: true },
        data: {
          id: toId(),
          address: input.address,
          apartment: input.apartment,
          city: input.city,
          country: input.country,
          default: true,
          createdAt: new Date(),
          phone: strToPhone(input.phone),
          state: input.state,
          zipCode: input.zipCode
        }
      })
    }
    return hasAddress
  }

  private calculateSaleOffPrice = (price = 0, saleOff = 0) => {
    if (saleOff === 0) return price

    return price - price * (saleOff / 100)
  }

  private async validateProducts(products: { slug: string; options: { quantity: number; options: string[] }[] }[]) {
    const items = await this._prisma.product.findMany({
      where: { isActive: true, slug: { in: products.map((p) => p.slug) } },
      select: {
        id: true,
        slug: true,
        productsCurrencies: {
          select: { productId: true, price: true, saleOff: true, currency: true },
          where: { currency: 'PHP' }
        },
        productsOptionsLinks: {
          select: { option: { select: { code: true } } }
        }
      }
    })

    if (items.length !== products.length) {
      return { valid: false, items: null }
    }

    let valid = false
    for (const p of products) {
      const i = items.find((i) => i.slug === p.slug)
      for (const { options } of p.options) {
        valid = options.every((o) => i?.productsOptionsLinks.some((l) => l.option?.code === o))
        if (!valid) {
          return { valid, items: null }
        }
      }
    }

    return { valid, items }
  }
}
