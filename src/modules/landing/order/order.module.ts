import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common'
import { JWTMiddleware } from '../jwt/JWTMiddleware'
import { OrderController } from './order.controller'
import { OrderService } from './order.service'

@Module({
  controllers: [OrderController],
  providers: [OrderService]
})
export class OrderModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JWTMiddleware)
      .forRoutes(
        { path: 'landing/order/find', method: RequestMethod.GET },
        { path: 'landing/order', method: RequestMethod.POST }
      )
  }
}
