import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { CreateOrderDto } from './order.dto'
import { OrderService } from './order.service'

import type { IListQuery } from '@/modules/base/dto'
import type { FastifyRequest } from 'fastify'

@UseInterceptors(CacheInterceptor)
@Controller('landing/order')
export class OrderController {
  constructor(private readonly service: OrderService) {}

  @Post()
  create(@Body() body: CreateOrderDto, @Req() req: FastifyRequest) {
    return this.service.create(body, req['ip'], req.raw['user'])
  }

  @Get('find')
  getOrders(@Query() query: IListQuery, @Req() req: FastifyRequest) {
    return this.service.getOrders(query, req.raw['user']['id'])
  }
}
