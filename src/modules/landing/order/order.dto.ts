import { isPhoneValid } from '@/modules/base/util'
import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const CreateOrderSchema = z.object({
  email: z.string().max(30).email(),
  firstname: z
    .string()
    .max(50)
    .regex(/^[\w\-.\s]+$/i, 'Value is not valid'),
  lastname: z
    .string()
    .max(50)
    .regex(/^[\w\-.\s]+$/i, 'Value is not valid'),
  address: z.string().max(50),
  apartment: z.string().max(50).optional().nullable(),
  state: z.string().max(50),
  city: z.string().max(50),
  zipCode: z.string().max(20),
  phone: z
    .string()
    .max(30)
    .refine((phone) => isPhoneValid(phone), { path: ['phone'], message: 'Phone is not valid' }),
  country: z.string().max(50),
  textMe: z.boolean().optional().nullable(),
  subscription: z.boolean().optional().nullable(),
  products: z
    .object({
      slug: z.string(),
      options: z
        .object({
          quantity: z.number({ invalid_type_error: 'Quantity must be a number' }).positive(),
          options: z.string().array()
        })
        .array()
    })
    .array(),
  ip: z
    .string()
    .regex(
      /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/,
      'Value is not valid'
    )
    .optional()
    .nullable(),
  source: z.string().max(2048).optional().nullable()
})
export class CreateOrderDto extends createZodDto(CreateOrderSchema) {}
