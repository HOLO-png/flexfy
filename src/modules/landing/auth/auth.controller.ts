import { Body, Controller, Get, Post, Req, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { FastifyRequest } from 'fastify'
import { ConfirmDto, SignInDto, SignUpDto } from './auth.dto'
import { AuthService } from './auth.service'

@Controller('landing/auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private jwtService: JwtService) {}

  @Get('me')
  getProfile(@Req() req: FastifyRequest) {
    return this.authService.getProfile(req.raw['user']['id'])
  }

  @Post('sign-up')
  signup(@Body() body: SignUpDto) {
    return this.authService.signup(body)
  }

  @Post('sign-up/confirm')
  confirm(@Body() body: ConfirmDto) {
    return this.authService.confirm(body)
  }

  @Post('sign-in')
  async signin(@Body() body: SignInDto) {
    const user = await this.authService.signin(body)
    if (user) {
      const payload = { id: user.id, username: user.email, name: user.firstname + ' ' + user.lastname }
      return {
        token: await this.jwtService.signAsync(payload, {
          secret: process.env.LANDING_SECRET_TOKEN || 'LANDING_SECRET_TOKEN'
        }),
        name: user.firstname + ' ' + user.lastname,
        firstname: user.firstname,
        lastname: user.lastname
      }
    } else {
      throw new UnauthorizedException()
    }
  }
}
