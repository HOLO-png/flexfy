import { BaseService } from '@/modules/base/service'
import { toId } from '@/modules/base/util'
import { Injectable, Logger } from '@nestjs/common'
import { compareSync, hashSync } from 'bcryptjs'
import { randomBytes } from 'node:crypto'
import { Transporter, createTransport } from 'nodemailer'
import type SMTPTransport from 'nodemailer/lib/smtp-transport'
import { ConfirmDto, SignInDto, SignUpDto } from './auth.dto'

const FLEXFY_REGISTER = {
  TemplateName: 'FLEXFY_REGISTER',
  SubjectPart: 'Welcome to Flexfy!!!',
  TextPart: '',
  HtmlPart: `<!DOCTYPE html>
    <html lang="en">
      <body>
        <div style="font-family:sans-serif;margin: 0 auto;max-width:800px;color:#4d4d4d;font-size:17px">
          <p style="font-size:30px;color:#ff9d39">Welcome to Flexfy !!!</p>
          <p style="color:#0069ec">Hi {userName},</p>
          <p>Thank you for choosing Flexfy.</p>
          <p>Your <span style="color:#0069ec">{email}</span> is now registered with us. Please click the URL link <a href="{link}" style="color:#0069ec">here</a> to confirm your email address. </p>
          <p style="margin-bottom:10px">Best Regards,</p>
          <p>
            <img style="width:180px;height:100px;object-fit:cover;" src="{img}" />
          </p>
        </div>
      </body>
    </html>`
}

@Injectable()
export class AuthService extends BaseService {
  getProfile(id: string) {
    return this._prisma.user.findUnique({
      select: {
        firstname: true,
        lastname: true,
        confirmed: true,
        confirmedAt: true,
        phone: true,
        addresses: {
          select: {
            id: true,
            address: true,
            apartment: true,
            city: true,
            country: true,
            default: true,
            state: true,
            zipCode: true,
            phone: true
          }
        },
        usersPoints: {
          select: { balance: true, createdAt: true, point: true, type: true, lastBalance: true }
        }
      },
      where: { id }
    })
  }

  private readonly mailSender: Transporter<SMTPTransport.SentMessageInfo>
  private readonly logger: Logger = new Logger(AuthService.name)

  constructor() {
    super()
    this.mailSender = createTransport({
      host: process.env.BREVO_SMTP,
      port: process.env.BREVO_PORT ? +process.env.BREVO_PORT : 587,
      auth: { user: process.env.BREVO_MAIL, pass: process.env.BREVO_PASS }
    })
  }

  async signup(input: SignUpDto) {
    const { email, password, ...rest } = input
    const user = await this._prisma.user.findUnique({
      select: { id: true, confirmationToken: true, confirmed: true },
      where: { email: input.email }
    })
    if (user && user.confirmed) {
      return this.withBadRequest(['email: Already used'])
    }

    return this._prisma.$transaction(
      async (tx) => {
        const createdAt = new Date()
        const confirmationToken = randomBytes(20).toString('hex')

        if (user) {
          await tx.user.update({
            where: { id: user.id },
            data: { id: toId(), ...rest, email, updatedAt: createdAt, confirmationToken, password: hashSync(password) },
            select: { createdAt: true }
          })
        } else {
          await tx.user.create({
            data: { id: toId(), ...rest, email, createdAt, confirmationToken, password: hashSync(password) },
            select: { createdAt: true }
          })
        }
        setTimeout(() => {
          // send confirmation email
          this.sendConfirmation(email, `${rest.firstname} ${rest.lastname}`, confirmationToken)
        }, 0)
        return { createdAt }
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  async confirm(input: ConfirmDto) {
    const { email, token } = input
    const user = await this._prisma.user.findFirst({
      where: { email: input.email, confirmationToken: token },
      select: { confirmed: true }
    })
    if (!user) {
      return this.withBadRequest(['token: Token invalid'])
    }

    if (user.confirmed) {
      return this.withBadRequest(['email: Already confirmed'])
    }

    return this._prisma.$transaction(
      async (tx) => {
        const createdAt = new Date()
        const user = await tx.user.update({
          where: { email },
          data: {
            updatedAt: createdAt,
            confirmedAt: createdAt,
            confirmed: true,
            usersPoints: {
              create: {
                id: toId(),
                balance: 100,
                createdAt,
                lastBalance: 0,
                point: +(process.env.SIGNUP_POINT || '500'),
                type: 'ADD',
                version: 1
              }
            }
          },
          select: { confirmedAt: true }
        })

        return user
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  async signin(input: SignInDto) {
    const user = await this._prisma.user.findFirst({
      where: { email: input.email },
      select: { id: true, email: true, password: true, firstname: true, lastname: true, confirmed: true }
    })
    if (!user) {
      return this.withBadRequest('Incorrect email or password')
    }
    if (!!user.confirmed) {
      return compareSync(input.password, user.password || '')
        ? user
        : this.withBadRequest('Incorrect email or password')
    } else {
      return this.withBadRequest(
        'The account has not been confirmed, to use the account please confirm it in your mailbox.'
      )
    }
  }

  private async sendConfirmation(email: string, fullname: string, token: string) {
    if (this.mailSender) {
      const confirmationLink = process.env.CONFIRMATION_LINK + token
      try {
        await this.mailSender.sendMail({
          from: `"Flexfy - Customer Services" ${process.env.BREVO_MAIL}`,
          to: email,
          subject: FLEXFY_REGISTER.SubjectPart,
          html: FLEXFY_REGISTER.HtmlPart.replace('{userName}', fullname)
            .replace('{email}', email)
            .replace('{link}', confirmationLink)
            .replace('{img}', process.env.CONFIRMATION_LOGO || '')
        })
      } catch (error) {
        this.logger.debug(error.message)
        this.logger.verbose(error.stack)
      }
    }
  }
}
