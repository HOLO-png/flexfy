import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const SignUpSchema = z.object({
  firstname: z.string().max(50),
  lastname: z.string().max(50),
  email: z.string().max(30),
  password: z.string()
})
export class SignUpDto extends createZodDto(SignUpSchema) {}

const ConfirmSchema = z.object({ email: z.string().max(30), token: z.string() })
export class ConfirmDto extends createZodDto(ConfirmSchema) {}

const SignInSchema = z.object({ email: z.string().max(30), password: z.string() })
export class SignInDto extends createZodDto(SignInSchema) {}
