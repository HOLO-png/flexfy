import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common'
import { JWTMiddleware } from '../jwt/JWTMiddleware'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'

@Module({
  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(JWTMiddleware).forRoutes({ path: 'landing/auth/me', method: RequestMethod.GET })
  }
}
