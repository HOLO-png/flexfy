import { IListQuery } from '@/modules/base/dto'
import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const NUMBER_CONFIG = { invalid_type_error: 'Value must be a number' }
const ProductReviewSchema = z.object({
  rating: z.number(NUMBER_CONFIG),
  title: z.string().max(100),
  comment: z.string().max(256),
  sizePurchased: z.string().max(15).optional().nullable(),
  usualSize: z.string().max(15).optional().nullable(),
  trueSize: z.number(NUMBER_CONFIG).min(1).max(5).optional().nullable(),
  trueCup: z.number(NUMBER_CONFIG).min(1).max(5).optional().nullable(),
  pairsWellWith: z.string().max(100).optional().nullable()
})

export class ProductReviewDto extends createZodDto(ProductReviewSchema) {}

const ProductQuestionSchema = z.object({
  question: z.string().max(256)
})

export class ProductQuestionDto extends createZodDto(ProductQuestionSchema) {}

export interface IReviewFilter extends IListQuery {
  keyword: string
  trueSize: number
  trueCup: number
  sort: 'recent' | 'highest' | 'lowest'
}
