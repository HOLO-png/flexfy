import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common'
import { JWTMiddleware } from '../jwt/JWTMiddleware'
import { ProductController } from './product.controller'
import { ProductService } from './product.service'

@Module({
  controllers: [ProductController],
  providers: [ProductService]
})
export class ProductModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JWTMiddleware)
      .forRoutes(
        { path: 'landing/product/(.*)', method: RequestMethod.POST },
        { path: 'landing/product/order/(.*)', method: RequestMethod.GET }
      )
  }
}
