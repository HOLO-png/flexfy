import { BaseService } from '@/modules/base/service'
import { diff, toId } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { IReviewFilter, ProductQuestionDto, ProductReviewDto } from './product.dto'

import type { IPaginationDto, IProductQuery, IRawProductDto } from '../dto'

const MAX_ITEM = 12
const SELECT_FIELDS: Prisma.ProductSelect = {
  id: true,
  name: true,
  slug: true,
  images: true,
  createdAt: true,
  isFave: true,
  productRatings: {
    select: {
      rating: true
    },
    where: { approvedAt: { not: null } }
  },
  productsOptionsLinks: {
    where: { option: { isActive: true } },
    select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } }
  },
  productsCurrencies: { select: { price: true, currency: true, saleOff: true }, where: { currency: 'PHP' } }
}

@Injectable()
export class ProductService extends BaseService {
  async getSaleOff(query: IProductQuery) {
    const { skip, take } = this.withLimitOffset({ page: query.page, pageSize: query.pageSize })
    const { options } = query
    let whereClause = `SELECT DISTINCT p.id FROM products p $optionsJoin WHERE p.is_active = true`

    const sql = `SELECT 
          p.id,
          p.slug,
          p.name,
          p.images,
          p.is_fave "isFave",
          p.created_at "createdAt",
          pc.currency,
          pc.price,
          pc.sale_off "saleOff"
        FROM products p JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP' 
        WHERE p.id IN ($whereClause) AND sale_off > 0
        LIMIT ${take} OFFSET ${skip};`

    const sqlCount = `SELECT COUNT(*)
        FROM products p JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP' 
        WHERE p.id IN ($whereClause) AND sale_off > 0`
    let optIds: string[] = []

    if (options) {
      const opts = await this._prisma.productOption.findMany({
        select: { id: true },
        where: { code: { in: options }, isActive: true }
      })

      optIds = opts.map((o) => o.id)

      const optionsJoin = `JOIN (SELECT pol.product_id FROM products p JOIN products_options_links pol ON pol.product_id = p.id
        WHERE p.is_active = true AND pol.option_id IN (${optIds.map((o) => `'${o}'`).join(',')})
        GROUP BY pol.product_id HAVING COUNT(pol.product_id) = ${optIds.length}) as pol ON p.id = pol.product_id`

      whereClause = whereClause.replace('$optionsJoin', optionsJoin)
    } else {
      whereClause = whereClause.replace('$optionsJoin', '')
    }

    const [result, totalRecords] = await Promise.all([
      this._prisma.$queryRawUnsafe<unknown[]>(sql.replace('$whereClause', whereClause)),
      this._prisma.$queryRawUnsafe<{ count: string }[]>(sqlCount.replace('$whereClause', whereClause))
    ])
    const data = await Promise.all(
      result.map(async (r) => {
        const { id, createdAt, ...out } = r as { id: string; createdAt: Date }
        const [rating, opts] = await Promise.all([
          this._prisma.productRating.aggregate({
            where: { productId: id, approvedAt: { not: null } },
            _count: { id: true },
            _avg: { rating: true }
          }),
          this._prisma.productOptionLink.findMany({
            select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } },
            where: { productId: id, optionId: optIds.length > 0 ? { in: optIds } : undefined }
          })
        ])

        out['options'] = opts
          .sort((p: any, n: any) =>
            `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`)
          )
          .map((o) => {
            Object.assign(o.option || {}, { pid: id })
            return o.option
          })

        out['isNew'] = true
        out['totalRating'] = rating._count.id
        out['rating'] = rating._avg.rating ?? 0
        return out
      })
    )
    return { data, pagination: { page: skip + 1, pageSize: take, totalRecords: Number(BigInt(totalRecords[0].count)) } }
  }

  async getNewArrival(query: IProductQuery) {
    const { skip, take } = this.withLimitOffset({ page: query.page, pageSize: query.pageSize })
    const { page, pageSize, options } = query
    const now = new Date()
    const lastMonth = new Date()
    lastMonth.setDate(now.getDate() - 30)
    let whereClause = `SELECT DISTINCT p.id FROM products p $optionsJoin WHERE p.is_active = true AND p.created_at <= '${now.toISOString()}' AND p.created_at >= '${lastMonth.toISOString()}'`
    console.log('whereClause', whereClause)
    const sql = `SELECT 
          p.id,
          p.slug,
          p.name,
          p.images,
          p.is_fave "isFave",
          p.created_at "createdAt",
          pc.currency,
          pc.price,
          pc.sale_off "saleOff"
        FROM products p JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP' 
        WHERE p.id IN ($whereClause)
        LIMIT ${take} OFFSET ${skip};`
    const sqlCount = `SELECT COUNT(*) FROM ($whereClause) AS sub;`
    let optIds: string[] = []

    if (options) {
      const opts = await this._prisma.productOption.findMany({
        select: { id: true },
        where: { code: { in: options }, isActive: true }
      })

      optIds = opts.map((o) => o.id)

      const optionsJoin = `JOIN (SELECT pol.product_id FROM products p JOIN products_options_links pol ON pol.product_id = p.id
        WHERE p.is_active = true AND pol.option_id IN (${optIds.map((o) => `'${o}'`).join(',')})
        GROUP BY pol.product_id HAVING COUNT(pol.product_id) = ${optIds.length}) as pol ON p.id = pol.product_id`

      whereClause = whereClause.replace('$optionsJoin', optionsJoin)
    } else {
      whereClause = whereClause.replace('$optionsJoin', '')
    }

    console.log('first', sql.replace('$whereClause', whereClause))
    const [result, totalRecords] = await Promise.all([
      this._prisma.$queryRawUnsafe<unknown[]>(sql.replace('$whereClause', whereClause)),
      this._prisma.$queryRawUnsafe<{ count: string }[]>(sqlCount.replace('$whereClause', whereClause))
    ])

    const data = await Promise.all(
      result.map(async (r) => {
        const { id, createdAt, ...out } = r as { id: string; createdAt: Date }
        const [rating, opts] = await Promise.all([
          this._prisma.productRating.aggregate({
            where: { productId: id, approvedAt: { not: null } },
            _count: { id: true },
            _avg: { rating: true }
          }),
          this._prisma.productOptionLink.findMany({
            select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } },
            where: { productId: id, optionId: optIds.length > 0 ? { in: optIds } : undefined }
          })
        ])

        out['options'] = opts
          .sort((p: any, n: any) =>
            `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`)
          )
          .map((o) => {
            Object.assign(o.option || {}, { pid: id })
            return o.option
          })

        out['isNew'] = true
        out['totalRating'] = rating._count.id
        out['rating'] = rating._avg.rating ?? 0
        return out
      })
    )

    return { data, pagination: { page: page, pageSize: pageSize, totalRecords: Number(BigInt(totalRecords[0].count)) } }
  }

  async getBestSellers(query: IProductQuery) {
    const { skip, take } = this.withLimitOffset({ page: query.page, pageSize: query.pageSize })
    const { options } = query

    const sql = `SELECT 
          p.id,
          p.slug,
          p.name,
          p.images,
          p.is_fave "isFave",
          p.created_at "createdAt",
          pc.currency,
          pc.price,
          pc.sale_off "saleOff"
        FROM products p
        JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP'
        WHERE p.id IN ($whereClause) 
        LIMIT ${take} OFFSET ${skip};`

    const sqlBestSellingCount = `SELECT COUNT(*)
        FROM products p JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP' 
        WHERE is_best_selling = true`

    const sqlCount = `SELECT COUNT(*)
        FROM products p JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP' 
        WHERE p.id IN ($whereClause) and p.is_active = true;`
    let optIds: string[] = []

    const countSelling = await this._prisma.$queryRawUnsafe<{ count: string }[]>(
      sqlBestSellingCount.replace('$whereClause', '')
    )
    let whereClause = `SELECT DISTINCT p.id FROM products p $optionsJoin WHERE p.is_active = true`
    if (Number(BigInt(countSelling[0]?.count)) <= 0) {
      const topOrder = await this._prisma.$queryRawUnsafe<unknown[]>(
        `SELECT "product_id" FROM "orders_products_links" GROUP BY "product_id" ORDER BY COUNT(*) DESC LIMIT ${Number(
          take
        )}`
      )
      whereClause = `SELECT DISTINCT p.id FROM products p $optionsJoin WHERE p.is_active = true AND p.id IN (${topOrder.map(
        (t: any) => `'${t?.product_id}'`
      )})`
    } else {
      whereClause = `SELECT DISTINCT p.id FROM products p $optionsJoin WHERE p.is_active = true is_best_selling = true`
    }
    if (options) {
      const opts = await this._prisma.productOption.findMany({
        select: { id: true },
        where: { code: { in: options }, isActive: true }
      })

      optIds = opts.map((o) => o.id)

      const optionsJoin = `JOIN (SELECT pol.product_id FROM products p JOIN products_options_links pol ON pol.product_id = p.id
        WHERE p.is_active = true AND pol.option_id IN (${optIds.map((o) => `'${o}'`).join(',')})
        GROUP BY pol.product_id HAVING COUNT(pol.product_id) = ${optIds.length}) as pol ON p.id = pol.product_id`

      whereClause = whereClause.replace('$optionsJoin', optionsJoin)
    } else {
      whereClause = whereClause.replace('$optionsJoin', '')
    }

    const [result, totalRecords] = await Promise.all([
      this._prisma.$queryRawUnsafe<unknown[]>(sql.replace('$whereClause', whereClause)),
      this._prisma.$queryRawUnsafe<{ count: string }[]>(sqlCount.replace('$whereClause', whereClause))
    ])

    const data = await Promise.all(
      result.map(async (r) => {
        const { id, createdAt, ...out } = r as { id: string; createdAt: Date }
        const [rating, opts] = await Promise.all([
          this._prisma.productRating.aggregate({
            where: { productId: id, approvedAt: { not: null } },
            _count: { id: true },
            _avg: { rating: true }
          }),
          this._prisma.productOptionLink.findMany({
            select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } },
            where: { productId: id, optionId: optIds.length > 0 ? { in: optIds } : undefined }
          })
        ])

        out['options'] = opts
          .sort((p: any, n: any) =>
            `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`)
          )
          .map((o) => {
            Object.assign(o.option || {}, { pid: id })
            return o.option
          })

        out['isNew'] = true
        out['totalRating'] = rating._count.id
        out['rating'] = rating._avg.rating ?? 0
        return out
      })
    )
    return {
      data,
      pagination: { page: skip + 1, pageSize: take, totalRecords: Number(BigInt(totalRecords[0].count)) }
    }
  }

  async findQuestions(slug: string, query: IReviewFilter): Promise<IPaginationDto> {
    const { skip, take } = this.withLimitOffset(query)
    const where: Prisma.ProductQuestionWhereInput = { repliedAt: { not: null }, product: { slug, isActive: true } }
    const orderBy: Prisma.ProductOptionOrderByWithRelationInput = { createdAt: 'desc' }

    if (query.keyword) {
      where.question = { contains: query.keyword, mode: 'insensitive' }
    }

    const [result, total] = await Promise.all([
      this._prisma.productQuestion.findMany({
        select: {
          author: true,
          email: true,
          question: true,
          createdAt: true,
          replied: true,
          repliedAt: true,
          repliedBy: { select: { firstname: true, lastname: true } }
        },
        where,
        skip,
        take,
        orderBy
      }),
      this._prisma.productQuestion.count({ where })
    ])

    const data = await Promise.all(
      result.map(async (r) => {
        const verifiedBuyer = await this._prisma.order.count({ where: { user: { email: r.email } } })
        r['verifiedBuyer'] = verifiedBuyer >= 1
        return r
      })
    )

    return { data, pagination: { page: query.page, pageSize: query.pageSize, totalRecords: total } }
  }

  async addQuestion(slug: string, input: ProductQuestionDto & { author: string; email: string }) {
    const product = await this._prisma.product.findUnique({ select: { id: true }, where: { slug, isActive: true } })
    if (!product) {
      return this.withBadRequest(['product: Product is not valid'])
    }
    const createdAt = new Date()
    const data: Prisma.ProductQuestionUncheckedCreateInput = { id: toId(), productId: product.id, createdAt, ...input }
    return this._prisma.productQuestion.create({ data, select: { createdAt: true } })
  }

  async addReview(slug: string, input: ProductReviewDto & { author: string; email: string }) {
    const [product, order] = await Promise.all([
      this._prisma.product.findUnique({
        select: { id: true },
        where: { slug, isActive: true }
      }),
      this._prisma.order.count({
        where: {
          user: { email: input.email },
          confirmedAt: { not: null },
          ordersProductsLinks: { some: { product: { slug: slug } } }
        }
      })
    ])
    if (order <= 0) {
      return this.withBadRequest('You cannot rate this product, please buy it to rate!!')
    }
    if (!product) {
      return this.withBadRequest(['product: Product is not valid'])
    }

    const createdAt = new Date()
    const data: Prisma.ProductRatingUncheckedCreateInput = { id: toId(), productId: product.id, createdAt, ...input }
    return this._prisma.productRating.create({ data, select: { createdAt: true } })
  }

  async findReviewSummary(slug: string) {
    const [reviews, totalQuestions] = await Promise.all([
      this._prisma.productRating.findMany({
        select: { rating: true, trueCup: true, trueSize: true },
        where: { approvedById: { not: null }, product: { slug, isActive: true } }
      }),
      this._prisma.productQuestion.count({
        where: { product: { slug, isActive: true }, repliedAt: { not: null } }
      })
    ])

    let rating5 = 0
    let rating4 = 0
    let rating3 = 0
    let rating2 = 0
    let rating1 = 0
    let trueSize = 0
    let trueCup = 0
    let totalRating = 0
    reviews.forEach((r) => {
      if (r.rating === 5) rating5++
      if (r.rating === 4) rating4++
      if (r.rating === 3) rating3++
      if (r.rating === 2) rating2++
      if (r.rating === 1) rating1++
      trueSize += r.trueSize || 1
      trueCup += r.trueCup || 1
      totalRating += r.rating
    })
    return {
      totalQuestions,
      totalReviews: reviews.length,
      average: Number((totalRating / reviews.length).toFixed(1)) || 0,
      totalFive: rating5,
      totalFour: rating4,
      totalThree: rating3,
      totalTwo: rating2,
      totalOne: rating1,
      trueCup: Number((trueCup / reviews.length).toFixed(2)) || 0,
      trueSize: Number((trueSize / reviews.length).toFixed(2)) || 0
    }
  }
  async getOrderBySlug(slug: string, author: { author: string; email: string }) {
    const orderList = await this._prisma.order.findMany({
      select: { id: true, ordersProductsLinks: { select: { product: true } } },
      where: {
        user: { email: author?.email?.toString() },
        confirmedAt: { not: null },
        ordersProductsLinks: { some: { product: { slug: slug } } }
      }
    })

    return orderList
  }

  async findReviews(slug: string, query: IReviewFilter): Promise<IPaginationDto> {
    const { skip, take } = this.withLimitOffset(query)

    const where: Prisma.ProductRatingWhereInput = {
      approvedAt: { not: null },
      product: { slug, isActive: true }
    }
    let orderBy: Prisma.ProductRatingOrderByWithRelationInput = { createdAt: 'desc' }

    if (query.sort && query.sort !== 'recent') {
      orderBy = { rating: query.sort === 'highest' ? 'desc' : 'asc' }
    }
    if (query.trueCup) {
      where.trueCup = query.trueCup
    }
    if (query.trueSize) {
      where.trueSize = query.trueSize
    }
    if (query.keyword) {
      where.comment = { contains: query.keyword, mode: 'insensitive' }
    }

    const [result, total] = await Promise.all([
      this._prisma.productRating.findMany({
        select: {
          author: true,
          email: true,
          title: true,
          rating: true,
          comment: true,
          trueCup: true,
          trueSize: true,
          sizePurchased: true,
          usualSize: true,
          pairsWellWith: true,
          createdAt: true
        },
        where,
        skip,
        take,
        orderBy
      }),
      this._prisma.productRating.count({ where })
    ])

    const data = await Promise.all(
      result.map(async (r) => {
        const verifiedBuyer = await this._prisma.order.count({ where: { user: { email: r.email } } })
        const orders = await this._prisma.order.findMany({ where: { user: { email: result[0].email } } })
        console.log(orders)

        r['verifiedBuyer'] = verifiedBuyer >= 1
        return r
      })
    )

    return { data, pagination: { page: query.page, pageSize: query.pageSize, totalRecords: total } }
  }

  async findOptions() {
    const options = await this._prisma.productOption.findMany({
      where: { isActive: true },
      select: { name: true, code: true, color: true, type: true }
    })

    const output: Record<string, any[]> = {}
    options.forEach((o) => {
      const type = o.type || ''
      const oldValue = output[type] || []
      output[type] = oldValue.concat([o])
    })

    return output
  }

  async find(query: IProductQuery) {
    const { skip, take } = this.withLimitOffset({ page: query.page, pageSize: query.pageSize })
    const { page, pageSize, options, category, saleOff, saleOffTo } = query

    let whereClause = `SELECT DISTINCT p.id FROM products p $categoryJoin $optionsJoin WHERE p.is_active = true`

    const sql = `SELECT 
          p.id,
          p.slug,
          p.name,
          p.images,
          p.is_fave "isFave",
          p.created_at "createdAt",
          pc.currency,
          pc.price,
          pc.sale_off "saleOff"
        FROM products p JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP' 
        ${saleOff ? 'AND pc.sale_off = ' + saleOff : ''}
        ${saleOffTo ? 'AND pc.sale_off <= ' + saleOffTo + ' AND pc.sale_off > 0' : ''}
        WHERE p.id IN ($whereClause)
        LIMIT ${take} OFFSET ${skip};`
    const sqlCount = `SELECT COUNT(*) FROM ($whereClause) AS sub;`

    let optIds: string[] = []

    if (options) {
      const opts = await this._prisma.productOption.findMany({
        select: { id: true },
        where: { code: { in: options }, isActive: true }
      })

      optIds = opts.map((o) => o.id)

      const optionsJoin = `JOIN (SELECT pol.product_id FROM products p JOIN products_options_links pol ON pol.product_id = p.id
        WHERE p.is_active = true AND pol.option_id IN (${optIds.map((o) => `'${o}'`).join(',')})
        GROUP BY pol.product_id HAVING COUNT(pol.product_id) = ${optIds.length}) as pol ON p.id = pol.product_id`

      whereClause = whereClause.replace('$optionsJoin', optionsJoin)
    } else {
      whereClause = whereClause.replace('$optionsJoin', '')
    }

    if (category) {
      const categories = await this._prisma.category.findMany({
        select: { id: true },
        where: { slug: { in: category }, isActive: true }
      })

      if (categories.length === 0) {
        return { data: [], pagination: { page: page, pageSize: pageSize, totalRecords: 0 } }
      }

      const parentIds = categories.map((c) => c.id)
      const subs = await this._prisma.category.findMany({
        select: { id: true },
        where: { parentId: { in: parentIds }, isActive: true }
      })

      const cIds = parentIds.concat(subs.map((s) => s.id))
      const categoryJoin = `JOIN products_categories_links pcl ON pcl.product_id = p.id 
        AND pcl.category_id IN (${cIds.map((c) => `'${c}'`).join(',')})`
      whereClause = whereClause.replace('$categoryJoin', categoryJoin)
    } else {
      whereClause = whereClause.replace('$categoryJoin', '')
    }

    const [result, totalRecords] = await Promise.all([
      this._prisma.$queryRawUnsafe<unknown[]>(sql.replace('$whereClause', whereClause)),
      this._prisma.$queryRawUnsafe<{ count: string }[]>(sqlCount.replace('$whereClause', whereClause))
    ])

    const data = await Promise.all(
      result.map(async (r) => {
        const { id, createdAt, ...out } = r as { id: string; createdAt: Date }
        const [rating, opts] = await Promise.all([
          this._prisma.productRating.aggregate({
            where: { productId: id, approvedAt: { not: null } },
            _count: { id: true },
            _avg: { rating: true }
          }),
          this._prisma.productOptionLink.findMany({
            select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } },
            where: { productId: id, optionId: optIds.length > 0 ? { in: optIds } : undefined }
          })
        ])
        out['options'] = opts
          .sort((p: any, n: any) =>
            `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`)
          )
          .map((o) => {
            Object.assign(o.option || {}, { pid: id })
            return o.option
          })

        out['isNew'] = diff(createdAt, 'month') <= 1
        out['totalRating'] = rating._count.id
        out['rating'] = rating._avg.rating ?? 0
        return out
      })
    )

    return { data, pagination: { page: page, pageSize: pageSize, totalRecords: Number(BigInt(totalRecords[0].count)) } }
  }

  async findOne(slug: string, preview?: string) {
    let where: Prisma.ProductWhereInput = { slug, isActive: true, productsCurrencies: { some: { currency: 'PHP' } } }

    if (preview) {
      where = { slug }
    }

    const detail = await this._prisma.product.findFirst({
      where,
      select: {
        id: true,
        slug: true,
        name: true,
        images: true,
        description: true,
        productsOptionsLinks: {
          select: { option: { select: { code: true, color: true, name: true, type: true } } }
        },
        productsCurrencies: { where: { currency: 'PHP' }, select: { price: true, saleOff: true, currency: true } }
      }
    })

    if (!detail) {
      throw new NotFoundException()
    }

    const { id, images, productsOptionsLinks, productsCurrencies, ...output } = detail
    const { _avg, _count } = await this._prisma.productRating.aggregate({
      where: { productId: id, approvedAt: { not: null } },
      _count: { id: true },
      _avg: { rating: true }
    })
    const options = productsOptionsLinks.sort((p, n) =>
      `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`)
    )
    // TODO get in database
    output['keyFeatures'] = [
      { title: 'wirefree comfort', description: 'full support sans pokey wires' },
      { title: 'lightly padded cups', description: 'all the feels, no bulk' },
      { title: '“no-slip” silicone grip', description: 'stays up, stays put' }
    ]
    output['images'] = images?.split(',')
    output['colors'] = options.filter((o) => o.option?.type === 'COLOR')

    output['band'] = options.filter((o) => o.option?.type === 'BAND')
    output['cup'] = options.filter((o) => o.option?.type === 'CUP')
    output['totalRating'] = _count.id
    output['rating'] = _avg.rating
    Object.assign(output, productsCurrencies[0])
    return output
  }

  async findRelated(slug: string) {
    const sql = Prisma.sql`SELECT p.id, p.is_active, p.slug, p.name, p.images, p.sku, pc.price, pc.sale_off FROM products p
        JOIN products_currencies pc ON pc.product_id = p.id AND pc.currency = 'PHP'
        JOIN products_categories_links pcl ON pcl.product_id = p.id
        JOIN categories c ON c.id = pcl.category_id
            AND pcl.category_id IN (SELECT pcl.category_id FROM products_categories_links pcl
            JOIN products p ON p.id = pcl.product_id AND p.slug = '${Prisma.raw(slug)}' AND p.is_active = TRUE)
        WHERE p.is_active = true AND p.slug <> '${Prisma.raw(slug)}' 
        GROUP BY p.id, pc.price, pc.sale_off LIMIT 6;`
    const relatedProducts = await this._prisma.$queryRaw<IRawProductDto[]>(sql)
    return Promise.all(
      relatedProducts.map(async (r) => {
        const { id, images, sale_off, ...output } = r

        const [rating, options] = await Promise.all([
          this._prisma.productRating.aggregate({
            where: { productId: r.id, approvedAt: { not: null } },
            _count: { id: true },
            _avg: { rating: true }
          }),
          this._prisma.productOption.findMany({
            select: { id: true, code: true, name: true, type: true, color: true },
            where: { isActive: true, productsOptionsLinks: { some: { productId: r.id } } }
          })
        ])
        const { _avg, _count } = rating
        output['totalRating'] = _count.id || 0
        output['rating'] = _avg.rating || 0
        output['images'] = images
        output['options'] = options.sort((p, n) => `${p?.type}${p?.code}`.localeCompare(`${n?.type}${n?.code}`))
        output['saleOff'] = sale_off
        return output
      })
    )
  }
  private handleRating(result: any[]) {
    return result.reduce((sum, item) => sum + item.rating, 0)
  }
}
