import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Param, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { parse } from 'qs'
import { IReviewFilter, ProductQuestionDto, ProductReviewDto } from './product.dto'
import { ProductService } from './product.service'

import type { FastifyRequest } from 'fastify'
import type { IProductQuery } from '../dto'

@UseInterceptors(CacheInterceptor)
@Controller('landing/product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get('find/sale-off')
  getSaleOff(@Query() query: IProductQuery) {
    return this.productService.getSaleOff(query as IProductQuery)
  }

  @Get('find/new-arrival')
  getNewArrival(@Query() query: IProductQuery) {
    return this.productService.getNewArrival(query as IProductQuery)
  }

  @Get('find/best-sellers')
  getBestSellers(@Query() query: IProductQuery) {
    return this.productService.getBestSellers(query as IProductQuery)
  }

  @Get('options')
  findOptions() {
    return this.productService.findOptions()
  }

  @Get('find')
  find(@Query() query: IProductQuery) {
    return this.productService.find(query as IProductQuery)
  }

  @Get('find/:slug')
  findOne(@Param('slug') slug: string, @Query() query: { preview?: string }) {
    return this.productService.findOne(slug, query.preview)
  }

  @Get('related/:slug')
  findRelated(@Param('slug') slug: string) {
    return this.productService.findRelated(slug)
  }

  @Get(':slug/review-summary')
  findReviewSummary(@Param('slug') slug: string) {
    return this.productService.findReviewSummary(slug)
  }

  @Get('order/:slug')
  getOrderBySlug(@Param('slug') slug: string, @Req() req: FastifyRequest) {
    const author = { author: req.raw['user']['name'], email: req.raw['user']['username'] }
    return this.productService.getOrderBySlug(slug, author)
  }

  @Get(':slug/reviews')
  findReview(@Param('slug') slug: string, @Req() req: FastifyRequest) {
    return this.productService.findReviews(slug, this.convertQuery(req))
  }

  @Post(':slug/reviews')
  submitReview(@Param('slug') slug: string, @Body() body: ProductReviewDto, @Req() req: FastifyRequest) {
    const author = { author: req.raw['user']['name'], email: req.raw['user']['username'] }
    return this.productService.addReview(slug, { ...body, ...author })
  }

  @Get(':slug/questions')
  findQuestions(@Param('slug') slug: string, @Req() req: FastifyRequest) {
    return this.productService.findQuestions(slug, this.convertQuery(req))
  }

  @Post(':slug/questions')
  submitQuestion(@Param('slug') slug: string, @Body() body: ProductQuestionDto, @Req() req: FastifyRequest) {
    const author = { author: req.raw['user']['name'], email: req.raw['user']['username'] }
    return this.productService.addQuestion(slug, { ...body, ...author })
  }

  private convertQuery(req: FastifyRequest): IReviewFilter {
    const query = parse(req.originalUrl.split('?')[1]) as IReviewFilter
    const { page, pageSize } = query
    query.page = page && +page > 0 ? +page : 1
    query.pageSize = pageSize && +pageSize > 0 ? +pageSize : 10
    return query
  }
}
