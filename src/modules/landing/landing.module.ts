import { Module } from '@nestjs/common'
import { AuthModule } from './auth/auth.module'
import { CategoryModule } from './category/category.module'
import { CountryModule } from './country/country.module'
import { HomeModule } from './home/home.module'
import { OrderModule } from './order/order.module'
import { ProductModule } from './product/product.module'
import { AddressModule } from './address/address.module'

@Module({
  imports: [AuthModule, HomeModule, OrderModule, CategoryModule, ProductModule, CountryModule, AddressModule]
})
export class LandingModule {}
