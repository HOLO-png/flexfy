import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Param, UseInterceptors } from '@nestjs/common'
import { HomeService } from './home.service'

@UseInterceptors(CacheInterceptor)
@Controller('landing/home')
export class HomeController {
  constructor(private readonly homeService: HomeService) {}

  @Get('banner')
  getBanner() {
    return this.homeService.getBanner()
  }

  @Get('promotion')
  getPromotion() {
    return this.homeService.getPromotion()
  }

  @Get('menu')
  getMenu() {
    return this.homeService.getMenu()
  }

  @Get('best-sellers')
  getBestSellers() {
    return this.homeService.getBestSellers()
  }

  @Get('sale-off-products')
  getSaleOffProducts() {
    return this.homeService.getSaleOffProducts()
  }

  @Get('sale-off-categories')
  getSaleOffCategories() {
    return this.homeService.getSaleOffCategories()
  }

  @Get('advertising')
  getAdvertising() {
    return this.homeService.getAdvertising()
  }

  @Get('community')
  getCommunity() {
    return this.homeService.getCommunity()
  }

  @Get('crew-raving')
  getReview() {
    return this.homeService.getCrewRaving()
  }

  @Get('footer')
  getFooter() {
    return this.homeService.getFooter()
  }

  @Get('static/:id')
  getStatic(@Param('id') id: string) {
    return this.homeService.getPage(id)
  }

  @Get('headers')
  async getHeaders() {
    return this.homeService.getHeaders()
  }

  @Get('robots')
  async getRobots() {
    return this.homeService.getRobots()
  }

  @Get('sitemap')
  async getSitemap() {
    return this.homeService.getSitemap()
  }

  @Get('ads-txt')
  async getAdsTxt() {
    return this.homeService.getAdsTxt()
  }
}
