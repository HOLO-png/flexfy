import { BaseService } from '@/modules/base/service'
import { diff, readFile } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { IAdvertising, IBanner, ICategories, IFooter, IPromotion } from '../dto'

@Injectable()
export class HomeService extends BaseService {
  getAdsTxt() {
    return readFile('ads', 'ads.txt')
  }

  getSitemap() {
    return readFile('sitemap', 'sitemap.xml')
  }

  getRobots() {
    return readFile('robots', 'robots.txt')
  }

  getHeaders() {
    return readFile('headers', 'headers.txt')
  }

  getBanner() {
    const data: Array<IBanner> = [
      {
        url: 'https://plus.unsplash.com/premium_photo-1682535209564-6dfca512c2bf?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1169&q=80',
        path: '/collections/sale-off'
      },
      {
        url: 'https://plus.unsplash.com/premium_photo-1677687191072-8b3156d30888?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2342&q=80',
        path: '/collections/sale-off'
      }
    ]
    return data
  }

  async getPromotion() {
    const data: IPromotion = {
      url: 'https://plus.unsplash.com/premium_photo-1679692887648-29823ba08032?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2340&q=80',
      path: '/rewards'
    }
    return data
  }

  async getMenu() {
    const sql = Prisma.sql`SELECT
      sub.name,
      sub.slug,
      sub.parent
    FROM
      (
        SELECT
          c.id,
          c.name,
          c.slug,
          (
            SELECT
              json_build_object('name', c2.name, 'slug', c2.slug)
            FROM
              categories c2
            WHERE
              c2.id = c.parent_id
          ) "parent",
          (
            SELECT
              COUNT(*)
            FROM
              products_categories_links pcl
            WHERE
              pcl.category_id = c.id
          ) "products"
        FROM
          categories c
        WHERE
          c.parent_id notnull
          AND c.is_active = true
      ) as sub
    WHERE
      sub.products > 0;`

    const result = await this._prisma.$queryRaw<
      {
        slug: string
        name: string
        parent: { slug: string; name: string }
      }[]
    >(sql)

    const menu: {
      slug: string
      name: string
      subMenu: { slug: string; name: string }[]
    }[] = []

    result.forEach(({ parent, ...rest }) => {
      const current = menu.find((s) => s.slug === parent.slug)
      if (current) {
        current.subMenu.push(rest)
      } else {
        menu.push({ ...parent, subMenu: [rest] })
      }
    })

    return menu
  }

  async getBestSellers() {
    const result = await this._prisma.product.findMany({
      select: {
        id: true,
        name: true,
        slug: true,
        images: true,
        createdAt: true,
        isFave: true,
        productsOptionsLinks: {
          select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } }
        },
        productsCurrencies: { select: { price: true, currency: true, saleOff: true }, where: { currency: 'PHP' } }
      },
      where: { isActive: true },
      take: 6
    })

    return this.handleResult(result)
  }

  async getSaleOffProducts() {
    // TODO handle get by currency
    const result = await this._prisma.product.findMany({
      select: {
        id: true,
        name: true,
        slug: true,
        images: true,
        productsOptionsLinks: {
          select: { option: { select: { id: true, code: true, name: true, type: true, color: true } } }
        },
        productsCurrencies: {
          select: { price: true, currency: true, saleOff: true },
          where: { currency: 'PHP' }
        }
      },
      where: {
        isActive: true,
        productsCurrencies: { some: { currency: 'PHP', saleOff: { gt: 0, lte: 30 } } }
      },
      take: 6
    })

    return this.handleResult(result)
  }

  async getSaleOffCategories() {
    const res = await this._prisma.productCurrency.findMany({
      select: {
        saleOff: true,
        product: {
          select: {
            categoriesLinks: {
              select: { category: { select: { parent: true, parentId: true, name: true, banner: true, slug: true } } },
              where: { category: { isActive: true, parent: { isMain: true, isActive: true } } }
            }
          }
        }
      },
      where: {
        saleOff: { gt: 0, lte: 15 },
        currency: 'PHP',
        product: {
          isActive: true,
          categoriesLinks: { every: { categoryId: { not: null } } }
        }
      },
      orderBy: { saleOff: 'desc' },
      take: 5
    })
    const dataSource: Partial<ICategories>[] = []
    res.forEach((r) => {
      const categoriesLinks = r.product.categoriesLinks.flat()
      categoriesLinks.forEach((c) => {
        if (!dataSource.some((d) => d.name === c.category?.name || d.name === c.category?.parent?.name)) {
          if (!!c.category?.parentId) {
            dataSource.push({
              saleOff: r.saleOff,
              name: c.category?.parent?.name,
              path: '/' + c.category?.parent?.slug,
              url: c.category?.parent?.banner?.split(',')[0]
            })
          } else {
            dataSource.push({
              saleOff: r.saleOff,
              name: c.category?.name,
              path: '/' + c.category?.slug,
              url: c.category?.banner?.split(',')[0]
            })
          }
        }
      })
    })

    return dataSource
  }

  async getCrewRaving() {
    return this._prisma.productRating.findMany({
      select: {
        author: true,
        rating: true,
        comment: true,
        product: {
          select: { images: true, name: true, slug: true }
        }
      },
      where: { approvedAt: { not: null }, product: { isActive: true } },
      orderBy: { rating: 'desc' },
      take: 7
    })
  }

  async getAdvertising() {
    const data: Array<IAdvertising> = [
      {
        url: 'https://images.unsplash.com/photo-1518605360659-2aa9659ef66d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2033&q=80',
        path: '/collections/new-arrival',
        name: 'New arrival'
      },
      {
        url: 'https://images.unsplash.com/photo-1526404746352-668ded9b50ab?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2340&q=80',
        path: '/collections/bra',
        name: 'strapless'
      }
    ]
    return data
  }

  async getCommunity() {
    const data: Array<string> = [
      'https://images.unsplash.com/photo-1656466610779-72619cbc6ad7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=987&q=80',
      'https://images.unsplash.com/photo-1602237778252-f3baa6486c59?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=987&q=80',
      'https://images.unsplash.com/photo-1638321155858-4d8150d502ff?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=987&q=80',
      'https://images.unsplash.com/photo-1637526997367-d44a21b2c3c3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=987&q=80'
    ]
    return data
  }

  async getFooter() {
    const categories = await this._prisma.category.findMany({
      select: { name: true, slug: true },
      where: { isActive: true, isMain: true }
    })
    const data: Array<IFooter> = [
      {
        name: 'SHOP',
        menu: categories
      },
      {
        name: 'COMMUNITY',
        menu: [
          { name: 'about us', slug: '/about-us' }
          // { name: 'events', slug: '/events' },
          // { name: 'the flexfy blog', slug: '/flexfy-blog' },
          // { name: 'join our rewards program ', slug: '/rewards' }
        ]
      },
      {
        name: 'LEARN',
        menu: [
          { name: 'fit guide', slug: '/fit-guide' },
          { name: 'size charts', slug: '/size-charts' },
          { name: 'contact us', slug: '/contact-us' },
          { name: 'faq', slug: '/faq' }
        ]
      },
      {
        name: 'RESOURCES',
        menu: [
          // { name: 'returns & exchanges (us)', slug: '/exchanges' },
          { name: 'privacy & terms', slug: '/privacy-terms' }
          // { name: 'careers', slug: '/careers' }
        ]
      }
    ]
    return data
  }

  getPage(slug: string) {
    return this._prisma.staticPages.findFirst({ where: { slug }, select: { html: true } })
  }

  private async handleResult(result: any[]) {
    return Promise.all(
      result.map(async (p: { [x: string]: any; productsOptionsLinks: any; productsCurrencies: any }) => {
        const { id, productsOptionsLinks, productsCurrencies, createdAt, ...out } = p
        out['options'] = productsOptionsLinks
          .sort((p, n) => `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`))
          .map((po: { option: { [x: string]: any; id: any } }) => {
            if (po.option) {
              po.option['pid'] = po.option?.id
            }
            return po.option
          })
        out['isNew'] = diff(createdAt, 'month') <= 1
        const ratingRs = await this._prisma.productRating.aggregate({
          where: { productId: id, approvedAt: { not: null } },
          _avg: { rating: true },
          _count: { productId: true }
        })
        out['totalRating'] = ratingRs._count.productId ?? 0
        out['rating'] = ratingRs._avg.rating ?? 0
        Object.assign(out, productsCurrencies[0])
        return out
      })
    )
  }
}
