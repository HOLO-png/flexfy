import { BaseService } from '@/modules/base/service'
import { toId } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { CreateAddressDto, EditAddressDto } from './address.dto'

@Injectable()
export class AddressService extends BaseService {
  async create(input: CreateAddressDto, user: { id: string; name: string }) {
    const checkExist = await this._prisma.userAddress.count({
      where: { address: input.address, userId: user.id }
    })
    if (checkExist > 0) {
      return this.withBadRequest(['address: Address is exist!!'])
    }

    const result = await this._prisma.userAddress.create({
      data: { ...input, id: toId(), userId: user.id, createdAt: new Date() },
      select: {
        id: true,
        address: true,
        apartment: true,
        city: true,
        country: true,
        default: true,
        state: true,
        zipCode: true,
        phone: true
      }
    })

    return result
  }

  async update(id: string, input: EditAddressDto, user: { id: string; name: string }) {
    const checkExist = await this._prisma.userAddress.count({
      where: { id: id, userId: user.id }
    })
    if (checkExist <= 0) {
      return this.withBadRequest(['address: Address is not exist!!'])
    }

    const result = await this._prisma.userAddress.update({
      where: { id: id, userId: user.id },
      data: { ...input, updatedAt: new Date() },
      select: {
        id: true,
        address: true,
        apartment: true,
        city: true,
        country: true,
        default: true,
        state: true,
        zipCode: true,
        phone: true
      }
    })
    return result
  }

  async delete(id: string, user: { id: string; name: string }) {
    const checkExist = await this._prisma.userAddress.count({
      where: { id: id, userId: user.id }
    })

    console.log('checkExist', checkExist)
    if (checkExist <= 0) {
      return this.withBadRequest(['address: Address is not exist!!'])
    }

    await this._prisma.userAddress.delete({
      where: { id: id, userId: user.id }
    })
    return true
  }
}
