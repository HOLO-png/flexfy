import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Param, Patch, Post, Req, UseInterceptors } from '@nestjs/common'
import { FastifyRequest } from 'fastify'
import { CreateAddressDto, EditAddressDto } from './address.dto'
import { AddressService } from './address.service'

@UseInterceptors(CacheInterceptor)
@Controller('landing/address')
export class AddressController {
  constructor(private readonly service: AddressService) {}

  @Post()
  create(@Body() body: CreateAddressDto, @Req() req: FastifyRequest) {
    return this.service.create(body, req.raw['user'])
  }

  @Patch('/:id')
  updateAddress(@Param('id') id: string, @Body() body: EditAddressDto, @Req() req: FastifyRequest) {
    return this.service.update(id, body, req.raw['user'])
  }

  @Delete('/:id')
  deleteAddress(@Param('id') id: string, @Req() req: FastifyRequest) {
    return this.service.delete(id, req.raw['user'])
  }
}
