import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common'
import { AddressController } from './address.controller'
import { AddressService } from './address.service'
import { JWTMiddleware } from '../jwt/JWTMiddleware'

@Module({
  controllers: [AddressController],
  providers: [AddressService]
})
export class AddressModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JWTMiddleware)
      .forRoutes(
        { path: 'landing/address', method: RequestMethod.POST },
        { path: 'landing/address/:id', method: RequestMethod.PATCH },
        { path: 'landing/address/:id', method: RequestMethod.DELETE }
      )
  }
}
