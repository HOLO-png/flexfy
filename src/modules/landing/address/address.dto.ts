import { isPhoneValid } from '@/modules/base/util'
import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const CreateAddressSchema = z.object({
  address: z.string().max(50),
  apartment: z.string().max(50).optional().nullable(),
  state: z.string().max(50),
  city: z.string().max(50),
  zipCode: z.string().max(20),
  phone: z
    .string()
    .max(30)
    .refine((phone) => isPhoneValid(phone), { path: ['phone'], message: 'Phone is not valid' }),
  country: z.string().max(50),
  default: z.boolean().optional().nullable()
})

const EditAddressSchema = z.object({
  address: z.string().max(50).optional(),
  apartment: z.string().max(50).optional().nullable(),
  state: z.string().max(50).optional(),
  city: z.string().max(50).optional(),
  zipCode: z.string().max(20).optional(),
  phone: z
    .string()
    .max(30)
    .refine((phone) => isPhoneValid(phone), { path: ['phone'], message: 'Phone is not valid' })
    .optional(),
  country: z.string().max(50),
  default: z.boolean().optional().nullable()
})
export class CreateAddressDto extends createZodDto(CreateAddressSchema) {}
export class EditAddressDto extends createZodDto(EditAddressSchema) {}
