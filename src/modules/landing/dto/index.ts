export interface IProductOptions {
  pid?: number
  code: string
  color: string
  thumbnail: string
}

export interface IProductItem {
  id?: number
  slug: string | null
  thumbnail: string
  rating: number
  totalRating: number
  name: string | null
  price: number
  isNew?: boolean
  isFave?: boolean
  options?: Array<IProductOptions>
  images?: string
  saleOff?: number
}

export interface IBanner {
  url: string
  path?: string
}

export interface ICategories {
  url: string | null
  path?: string | null
  name?: string | null
  saleOff?: number | null
}

export interface IReview {
  rating: number
  comment: string
  name: string
  product: IProductItem
}

export interface IAdvertising {
  url: string
  path?: string
  name?: string
}

export interface IPromotion {
  url: string
  path?: string
}

export interface IFooter {
  name: string
  menu: { slug: string | null; name: string | null }[]
}

export interface IPaginationDto {
  data: unknown[]
  pagination: IListQuery & { totalRecords: number }
}

export interface IListQuery {
  page: number
  pageSize: number
}

export interface IProductDto {
  price: number
  currency: string
  saleOff: number
  productId: number
  product: { name: string; slug: string; images: string }
}

export interface IProductQuery extends IListQuery {
  options?: string[]
  category?: string[]
  saleOff?: number
  saleOffTo?: number
}

export interface IRawProductDto {
  id: string
  price: number
  currency: string
  sale_off: number
  images: string
  slug: string
  name: string
}
