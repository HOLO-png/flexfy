import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, UseInterceptors } from '@nestjs/common'
import { CountryService } from './country.service'

@UseInterceptors(CacheInterceptor)
@Controller('landing/country')
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @Get('')
  findMany() {
    return this.countryService.findMany()
  }
}
