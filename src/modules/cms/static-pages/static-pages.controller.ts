import { BaseController } from '@/modules/base/controller'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Param, Post, Req, UsePipes } from '@nestjs/common'
import { StaticPageDto } from './static-pages.dto'
import { StaticPagesService } from './static-pages.service'

@UsePipes(CacheInterceptor)
@Controller('cms/static-pages')
export class StaticPagesController extends BaseController {
  constructor(private readonly service: StaticPagesService) {
    super()
  }

  @Get('robots')
  async getRobots() {
    return this.service.getRobots()
  }

  @Post('robots')
  async updateRobots(@Body() body: StaticPageDto) {
    return this.service.updateRobots(body.html || '')
  }

  @Get('headers')
  async getHeaders() {
    return this.service.getHeaders()
  }

  @Post('headers')
  async updateHeaders(@Body() body: StaticPageDto) {
    return this.service.updateHeaders(body.html || '')
  }

  @Get('ads-txt')
  async getAdsTxt() {
    return this.service.getAdsTxt()
  }

  @Post('ads-txt')
  async updateAdsTxt(@Body() body: StaticPageDto) {
    return this.service.updateAdsTxt(body.html || '')
  }

  @Get(':id')
  async getPage(@Param('id') id: string) {
    return this.service.getPage(id)
  }

  @Post(':id')
  async updatePage(@Param('id') id: string, @Body() body: StaticPageDto, @Req() req: Request) {
    return this.service.updatePage(id, this.withUpdated(body, req))
  }
}
