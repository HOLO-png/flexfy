import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

export enum OrderStatus {
  CONFIRMED = 'CONFIRMED',
  CANCELLED = 'CANCELLED',
  UPSELL = 'UPSELL',
  REDIAL = 'REDIAL',
  NEW = 'NEW',
  DUPLICATED = 'DUPLICATED',
  UPLOADED = 'UPLOADED'
}



export enum DeliveryStatus {
  DELIVERED = 'DELIVERED',
  RTS = 'RTS',
  VOID = 'VOID',
  IN_TRANSIT_HOLD = 'IN_TRANSIT_HOLD',
  IN_TRANSIT = 'IN_TRANSIT',
  FOR_PICK_UP = 'FOR_PICK_UP',
  RETURNED = 'RETURNED'
}

const UpdateOrderSchema = z.object({
  trackingNb: z.string().max(20).nullable().optional(),
  marketer: z.string().max(40).nullable().optional(),
  l3: z.number({ invalid_type_error: 'Must be a number' }).nonnegative(),
  l8: z.number({ invalid_type_error: 'Must be a number' }).nonnegative(),
  l3PHP: z.number({ invalid_type_error: 'Must be a number' }).nonnegative(),
  l8PHP: z.number({ invalid_type_error: 'Must be a number' }).nonnegative(),
  status: z.string().nullable().optional(),
  paymentStatus: z.string().nullable().optional(),
  deliveryStatus: z.string().nullable().optional(),
  note: z.string().max(512).nullable().optional(),
  partnerNote: z.string().max(512).nullable().optional(),
  cancelReason: z.string().max(256).nullable().optional()
})
export class UpdateOrderDto extends createZodDto(UpdateOrderSchema) {}
