import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Param, Patch, Query, Req, UseInterceptors } from '@nestjs/common'
import { UpdateOrderDto } from './order.dto'
import { OrderService } from './order.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/orders')
export class OrderController extends BaseController {
  constructor(private readonly orderService: OrderService) {
    super()
  }

  @Get('find')
  findMany(@Query() query: IListQuery) {
    return this.orderService.findMany(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateOrderDto, @Req() req: Request) {
    return this.orderService.update(id, this.withUpdated(body, req))
  }

  @Get('export')
  exportData(@Query() query: IListQuery) {
    return this.orderService.export(query)
  }
}
