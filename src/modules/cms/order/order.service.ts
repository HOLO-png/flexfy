import type { BaseDto, IFilter, IListDto, IListQuery } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { toCurrency, toId } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { Workbook } from 'exceljs'
import { readFileSync } from 'fs'
import { join } from 'path'
import { DeliveryStatus, OrderStatus, PaymentStatus, UpdateOrderDto } from './order.dto'

const ORDER_HEADERS = [
  'TIMESTAMP',
  'NAME',
  'ADDRESS',
  'PHONE',
  'NOTE',
  'PRODUCT NAME',
  'OPTIONS',
  'MKT-ER',
  'MARKET',
  'SOURCE',
  'LADI LINK',
  'OTHER',
  'ORDER ID',
  'C3',
  'STATUS',
  'L3',
  'L8',
  'DELIVERY STATUS',
  'PAYMENT STATUS',
  'CANCEL REASONS',
  'L3(PHP)',
  'L8(PHP)',
  'TRACKING NB',
  'QUANTITY',
  'NOTE'
]
@Injectable()
export class OrderService extends BaseService {
  private readonly filterable: Array<IFilter>

  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/order.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async export(query: IListQuery & { source?: string }) {
    const where = this.withFilter(query, this.filterable)
    if (query.source) {
      if (query.source === 'ORIGINAL') {
        where.AND?.push({ ladiPage: null })
      } else if (query.source === 'LADIPAGE') {
        where.AND?.push({ ladiPage: { not: null } })
      }
    }
    const dataSource = await this._prisma.order.findMany({
      select: {
        id: true,
        code: true,
        createdAt: true,
        totalAmount: true,
        totalProduct: true,
        status: true,
        trackingNb: true,
        marketer: true,
        paymentStatus: true,
        deliveryStatus: true,
        customerAddress: true,
        customerName: true,
        customerPhone: true,
        country: true,
        source: true,
        ladiPage: true,
        note: true,
        other: true,
        c3: true,
        l3: true,
        l3PHP: true,
        l8: true,
        l8PHP: true,
        partnerNote: true,
        cancelReason: true,
        user: { select: { id: true, firstname: true, lastname: true } },
        ordersProductsLinks: { select: { currency: true, options: true, product: { select: { name: true } } } }
      },
      where,
      orderBy: { createdAt: 'desc' }
    })

    const data: any[][] = [ORDER_HEADERS]
    const wb = new Workbook()
    const ws = wb.addWorksheet('Data')

    dataSource.forEach((d) => {
      const ordersProductsLinks = d.ordersProductsLinks
      const products: string[] = []
      const options: string[] = []
      const quantity: any[] = []
      ordersProductsLinks.forEach((op) => {
        if (op.product && op.product.name) {
          products.push(op.product.name)
        }
        if (op.options) {
          op.options.forEach((o) => {
            if (o && o['options']) options.push(o['options'])
            if (o && o['quantity']) quantity.push(o['quantity'])
          })
        }
      })
      const rows: any[] = []
      rows.push(d.createdAt) //TIMESTAMP
      rows.push(d.customerName) // NAME
      rows.push(d.customerAddress) // ADDRESS
      rows.push(d.customerPhone) // PHONE
      rows.push(d.note) // NOTE
      rows.push(products.join(', ')) // PRODUCT NAME
      rows.push(options.join(', ')) // OPTIONS
      rows.push(d.marketer) // MKT-ER
      rows.push(d.country) // MARKET
      rows.push(d.source) // SOURCE
      rows.push(d.ladiPage) // LADI LINK
      rows.push(d.other) // OTHER
      rows.push(d.code) // ORDER ID
      rows.push(d.c3) // C3
      rows.push(d.status) // STATUS
      rows.push(d.l3) // L3
      rows.push(d.l8) // L8
      rows.push(d.deliveryStatus) // DELIVERY STATUS
      rows.push(d.paymentStatus) // PAYMENT STATUS
      rows.push(d.cancelReason) // CANCEL REASONS
      rows.push(d.l3PHP) // L3(PHP)
      rows.push(d.l8PHP) // L8(PHP)
      rows.push(d.trackingNb) // TRACKING NB
      rows.push(quantity.join(',')) // QUANTITY
      rows.push(d.partnerNote) // NOTE
      data.push(rows)
    })

    ws.addRows(data)

    return wb.xlsx.writeBuffer()
  }

  async findMany(query: IListQuery & { source?: string }): Promise<IListDto> {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    if (query.source) {
      if (query.source === 'ORIGINAL') {
        where.AND?.push({ ladiPage: null })
      } else if (query.source === 'LADIPAGE') {
        where.AND?.push({ ladiPage: { not: null } })
      }
    }
    const [dataSource, total] = await Promise.all([
      this._prisma.order.findMany({
        select: {
          id: true,
          code: true,
          createdAt: true,
          totalAmount: true,
          totalProduct: true,
          status: true,
          trackingNb: true,
          marketer: true,
          paymentStatus: true,
          deliveryStatus: true,
          ip: true,
          user: { select: { id: true, firstname: true, lastname: true } },
          ordersProductsLinks: { select: { currency: true }, take: 1 }
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.order.count({ where })
    ])

    dataSource.forEach((d) => {
      d['key'] = d.id
      d['currency'] = d.ordersProductsLinks ? d.ordersProductsLinks[0]?.currency : null
      d['totalAmount'] = toCurrency(d.totalAmount) as any
      d['user'] = (
        d.user ? { id: d.user.id, label: `${d.user.firstname || ''} ${d.user.lastname || ''}`, path: '/users' } : null
      ) as any
    })
    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const detail = await this._prisma.order.findFirst({
      select: {
        id: true,
        code: true,
        createdAt: true,
        totalAmount: true,
        totalProduct: true,
        status: true,
        paymentStatus: true,
        deliveryStatus: true,
        ip: true,
        customerName: true,
        note: true,
        partnerNote: true,
        l3: true,
        l8: true,
        l3PHP: true,
        l8PHP: true,
        trackingNb: true,
        marketer: true,
        cancelReason: true,
        ordersProductsLinks: {
          select: {
            currency: true,
            price: true,
            amount: true,
            saleOff: true,
            product: { select: { id: true, name: true } },
            options: true
          }
        }
      },
      where: { id }
    })
    if (!detail) throw new NotFoundException()

    const { customerName, ordersProductsLinks, totalAmount, ...output } = detail
    output['totalAmount'] = toCurrency(totalAmount)
    output['createdBy'] = customerName
    output['currency'] = ordersProductsLinks ? ordersProductsLinks[0]?.currency : null
    output['products'] = ordersProductsLinks.map((p) => {
      const { product, options, ...rest } = p
      rest['product'] = product ? { id: product.id, label: product.name, path: '/products' } : null
      rest['options'] = options.map((o: any) => `Options: ${o.options.join(', ')} - Quantity: ${o.quantity}`).join('\n')
      rest['key'] = p.currency
      rest['amount'] = toCurrency(p.amount as any) as any
      rest['price'] = toCurrency(p.price as any) as any
      return rest
    })
    output['isCompleted'] = this.isOrderCompleted(output)
    return output
  }

  async update(id: string, input: UpdateOrderDto & BaseDto) {
    const order = await this._prisma.order.findFirst({
      where: { id },
      select: {
        status: true,
        paymentStatus: true,
        deliveryStatus: true,
        ladiPage: true,
        l3: true,
        l3PHP: true,
        l8: true,
        l8PHP: true
      }
    })

    if (!order) throw new NotFoundException()

    if (OrderStatus.DUPLICATED === order.status || OrderStatus.CANCELLED === order.status) {
      return this.withBadRequest(['status: Order is cancelled'])
    }

    // validate status
    if (this.isOrderCompleted(order)) {
      return this.withBadRequest(['status: Order is completed'])
    }

    if (order.paymentStatus !== input.paymentStatus && order.paymentStatus === PaymentStatus.PAID) {
      return this.withBadRequest(['paymentStatus: Order paid'])
    }
    if (order.deliveryStatus !== input.deliveryStatus && order.deliveryStatus === DeliveryStatus.DELIVERED) {
      return this.withBadRequest(['deliveryStatus: Order delivered'])
    }

    const data: Prisma.OrderUncheckedUpdateInput = {
      note: input.note,
      partnerNote: input.partnerNote,
      updatedAt: input.updatedAt,
      trackingNb: input.trackingNb,
      status: input.status,
      paymentStatus: input.paymentStatus,
      deliveryStatus: input.deliveryStatus,
      marketer: input.marketer
    }

    if (OrderStatus.CONFIRMED === input.status) {
      data.status = input.status
      data.confirmedAt = input.updatedAt
    }
    if (OrderStatus.CANCELLED === input.status) {
      data.cancelReason = input.cancelReason
      data.cancelAt = input.updatedAt
    }
    if (PaymentStatus.PAID === input.paymentStatus) {
      data.paidAt = input.updatedAt
    }
    if (DeliveryStatus.DELIVERED === input.deliveryStatus) {
      data.deliveredAt = input.updatedAt
    }

    if (order.ladiPage) {
      data.l3 = input.l3
      data.l3PHP = input.l3PHP
      data.l8 = input.l8
      data.l8PHP = input.l8PHP
      data.totalAmount = data.l3
    }

    // TODO send email to user when order completed
    return this._prisma.order.update({
      where: { id },
      data: {
        ...data,
        histories: {
          create: {
            id: toId(),
            createdAt: input['updatedAt'],
            action: [{ content: JSON.stringify(data), createdAt: input['updatedAt'] }]
          }
        }
      },
      select: { updatedAt: true }
    })
  }

  private isOrderCompleted(order: {
    status: string | null
    deliveryStatus: string | null
    paymentStatus: string | null
  }) {
    if (order.status === OrderStatus.CANCELLED || order.status === OrderStatus.DUPLICATED) {
      return true
    }

    return (
      order.status === OrderStatus.CONFIRMED &&
      order.deliveryStatus === DeliveryStatus.DELIVERED &&
      order.paymentStatus === PaymentStatus.PAID
    )
  }
}
