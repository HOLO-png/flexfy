import { Module } from '@nestjs/common'
import { APP_GUARD } from '@nestjs/core'
import { CMSAuthGuard } from '../guards/cms.guard'
import { AdminModule } from './admin/admin.module'
import { AuthModule } from './auth/auth.module'
import { CategoryModule } from './category/category.module'
import { DashboardModule } from './dashboard/dashboard.module'
import { OrderModule } from './order/order.module'
import { ProductOptionModule } from './product-option/product-option.module'
import { ProductQuestionsModule } from './product-questions/product-questions.module'
import { ProductRatingModule } from './product-rating/product-rating.module'
import { ProductModule } from './product/product.module'
import { StaticPagesModule } from './static-pages/static-pages.module'
import { UserModule } from './user/user.module'

@Module({
  imports: [
    AdminModule,
    AuthModule,
    CategoryModule,
    DashboardModule,
    OrderModule,
    ProductModule,
    ProductOptionModule,
    ProductRatingModule,
    ProductQuestionsModule,
    UserModule,
    StaticPagesModule
  ],
  providers: [{ provide: APP_GUARD, useClass: CMSAuthGuard }]
})
export class CmsModule {}
