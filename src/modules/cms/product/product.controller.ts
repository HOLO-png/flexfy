import { BaseController } from '@/modules/base/controller'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { CreateProductDto, IProductQuery, UpdateProductDto } from './product.dto'
import { ProductService } from './product.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/products')
export class ProductController extends BaseController {
  constructor(private readonly productService: ProductService) {
    super()
  }

  @Post()
  create(@Body() body: CreateProductDto, @Req() req: Request) {
    return this.productService.create(this.withUpdated(body, req))
  }

  @Get('find')
  async findMany(@Query() query: IProductQuery, @Req() req: Request) {
    return this.productService.findAll(query, req['user'])
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateProductDto, @Req() req: Request) {
    return this.productService.update(id, this.withUpdated(body, req))
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productService.remove(id)
  }

  @Get('rating')
  async findRating(@Query() query: IProductQuery, @Req() req: Request) {
    return this.productService.findRating(query, req['user'])
  }
}
