import type { BaseDto, CurrentUser, IFilter } from '@/modules/base/dto'
import { removeImageFile, saveFile, saveImageFile, toId, toSlug } from '@/modules/base/util'
import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common'
import { Prisma, ProductCurrency } from '@prisma/client'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { CreateProductDto, IProductQuery, UpdateProductDto } from './product.dto'

import xml from 'xml'

const DEFAULT_CURRENCIES = [
  { key: 'PHP', currency: 'PHP', price: 0, saleOff: 0, cost: 0 }
  // { key: 'USD', currency: 'USD', price: 0, saleOff: 0, cost: 0 },
  // { key: 'MYR', currency: 'MYR', price: 0, saleOff: 0, cost: 0 }
]

@Injectable()
export class ProductService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/product.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findRating(query: IProductQuery, arg1: any) {
    const { skip, take } = this.withLimitOffset(query)
    const [dataSource, total] = await Promise.all([
      this._prisma.productRating.findMany({
        select: {
          id: true,
          author: true,
          email: true,
          title: true,
          comment: true,
          createdAt: true,
          approvedAt: true,
          approvedBy: { select: { id: true, firstname: true, lastname: true } }
        },
        where: {},
        take,
        skip,
        orderBy: { createdAt: 'desc' }
      }),
      this._prisma.product.count({ where: {} })
    ])

    dataSource.forEach((x) => {
      x['key'] = x.id
      x['approvedBy'] = x.approvedBy
        ? ({
            id: x.approvedBy.id,
            label: `${x.approvedBy.firstname || ''} ${x.approvedBy.lastname || ''}`,
            path: '/users'
          } as any)
        : null
    })

    return this.withPagination(dataSource, query, total)
  }

  async create(input: CreateProductDto) {
    const { images: imagesUpload, options, categories, currencies, ...data } = input

    if (imagesUpload) {
      const images = await saveImageFile(imagesUpload, 'product')
      data['images'] = images.join(',')
      data['imagesUpload'] = imagesUpload
    }

    const output = await this._prisma.$transaction(
      async (tx) => {
        data['slug'] = await this.generateSlug(data.name)
        return tx.product.create({
          data: {
            ...data,
            id: toId(),
            productsOptionsLinks: {
              createMany: {
                data: options ? options.map((x) => ({ optionId: x, id: toId() })) : [],
                skipDuplicates: true
              }
            },
            categoriesLinks: {
              createMany: {
                data: categories ? categories.map((x) => ({ categoryId: x, id: toId() })) : [],
                skipDuplicates: true
              }
            },
            productsCurrencies: {
              createMany: {
                data: currencies ? currencies.map((x) => ({ ...x, id: toId() } as any)) : [],
                skipDuplicates: true
              }
            }
          },
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )

    this.generateSitemap()

    return output
  }

  async findAll(query: IProductQuery, user: CurrentUser) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    if (query.option) {
      const productsOptionsLinks: Prisma.ProductOptionLinkListRelationFilter = {
        some: { optionId: { in: query.option.map((o) => o) } }
      }
      where.AND?.push({ productsOptionsLinks })
    }
    if (query.category) {
      const categoriesLinks: Prisma.ProductCategoryLinkListRelationFilter = {
        some: { categoryId: { in: query.category.map((o) => o) } }
      }
      where.AND?.push({ categoriesLinks })
    }
    const [result, total] = await Promise.all([
      this._prisma.product.findMany({
        where,
        select: {
          id: true,
          slug: true,
          name: true,
          images: true,
          sku: true,
          isActive: true,
          isFave: true,
          createdAt: true,
          categoriesLinks: {
            select: { category: { select: { id: true, name: true } } },
            where: { category: { isActive: true } }
          },
          productsOptionsLinks: {
            select: { option: { select: { id: true, name: true } } },
            where: { option: { isActive: true } }
          },
          createdBy: { select: { id: true, firstname: true, lastname: true } }
        },
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.product.count({ where })
    ])

    // TODO handle permission later
    const isSuperAdmin = user.roles.includes('super-admin')
    const dataSource = result.map((record) => {
      const { createdBy, categoriesLinks, productsOptionsLinks, slug, images, ...x } = record
      x['key'] = x.id
      x['images'] = images?.split(',')[0]
      x['preview'] = process.env.SITEMAP_DOMAIN
        ? `${process.env.SITEMAP_DOMAIN}/products/${slug}?preview=${Date.now()}`
        : null
      x['deletable'] = isSuperAdmin || createdBy?.id === user.id
      x['category'] = categoriesLinks.map((c) => c.category?.name).join(', ')
      x['options'] = productsOptionsLinks.map((c) => c.option?.name).join(', ')
      if (createdBy) {
        x['createdBy'] = {
          id: createdBy.id,
          label: `${createdBy.firstname || ''} ${createdBy.lastname || ''}`,
          path: '/users'
        } as any
      }
      return x
    })

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const product = await this._prisma.product.findFirst({
      where: { id },
      select: {
        name: true,
        id: true,
        productsOptionsLinks: {
          select: { option: { select: { id: true, type: true, code: true } } },
          where: { option: { isActive: true } }
        },
        categoriesLinks: {
          select: { categoryId: true },
          where: { category: { isActive: true, isMain: { not: true } } }
        },
        createdAt: true,
        isActive: true,
        isFave: true,
        sku: true,
        slug: true,
        imagesUpload: true,
        description: true,
        productsCurrencies: { select: { currency: true, price: true, cost: true, saleOff: true } }
      }
    })
    if (!product) throw new NotFoundException()

    const { categoriesLinks, productsOptionsLinks, imagesUpload: images, productsCurrencies, ...output } = product
    output['options'] = productsOptionsLinks
      .sort((p, n) => `${p.option?.type}${p.option?.code}`.localeCompare(`${n.option?.type}${n.option?.code}`))
      .map((m) => m.option?.id)
    output['categories'] = categoriesLinks.map((category) => category.categoryId)
    output['images'] = images
    output['currencies'] = productsCurrencies.length
      ? productsCurrencies.map((p) => {
          p['key'] = p.currency
          return p
        })
      : DEFAULT_CURRENCIES

    return output
  }

  async update(id: string, input: Partial<UpdateProductDto & BaseDto>) {
    const { images: imagesUpload, options, categories, currencies, ...data } = input

    const detail = await this._prisma.product.findFirst({
      where: { id },
      select: {
        imagesUpload: true,
        categoriesLinks: { select: { id: true, categoryId: true } },
        productsCurrencies: { select: { id: true, currency: true } },
        productsOptionsLinks: { select: { id: true, optionId: true } }
      }
    })
    if (!detail) {
      throw new NotFoundException()
    }

    const removeOptions = detail.productsOptionsLinks.filter(({ optionId }) => !options?.some((c) => optionId === c))
    const removeCategories = detail.categoriesLinks.filter(
      ({ categoryId }) => !categories?.some((c) => categoryId === c)
    )

    const bulkActions: any[] = []

    if (imagesUpload) {
      const images = await saveImageFile(imagesUpload, 'product')
      data['images'] = images.join(',')
      data['imagesUpload'] = imagesUpload
    }
    const output = await this._prisma.$transaction(
      async (tx) => {
        if (removeOptions.length > 0) {
          bulkActions.push(tx.productOptionLink.deleteMany({ where: { id: { in: removeOptions.map((r) => r.id) } } }))
        }
        if (removeCategories.length > 0) {
          bulkActions.push(
            tx.productCategoryLink.deleteMany({ where: { id: { in: removeCategories.map((r) => r.id) } } })
          )
        }

        bulkActions.push(
          ...(currencies || []).map((c: any) => {
            const data: Partial<ProductCurrency & BaseDto> = {
              createdAt: input.createdAt,
              createdById: input.createdById,
              updatedById: input.updatedById,
              updatedAt: input.updatedAt,
              productId: id,
              id: toId(),
              ...c
            }
            return tx.productCurrency.upsert({
              where: { productId_currency: { productId: id, currency: c.currency } },
              create: data as any,
              update: data
            })
          })
        )

        await Promise.all(bulkActions)

        return tx.product.update({
          where: { id },
          data: {
            ...data,
            productsOptionsLinks: {
              createMany: {
                data: options ? options.map((x) => ({ optionId: x, id: toId() })) : [],
                skipDuplicates: true
              }
            },
            categoriesLinks: {
              createMany: {
                data: categories ? categories.map((x) => ({ categoryId: x, id: toId() })) : [],
                skipDuplicates: true
              }
            }
          },
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )

    this.generateSitemap()

    return output
  }

  async remove(id: string) {
    const product = await this._prisma.product.findFirst({
      where: { id },
      select: { images: true, ordersLinks: { select: { id: true }, take: 1 } }
    })
    if (!product) throw new NotFoundException()

    if (product.ordersLinks.length > 0) {
      throw new ForbiddenException('Cannot delete product incudes order')
    }

    const output = await this._prisma.$transaction(
      async (tx) => {
        await Promise.all([
          tx.productCurrency.deleteMany({ where: { productId: id } }),
          tx.productCategoryLink.deleteMany({ where: { productId: id } }),
          tx.productOptionLink.deleteMany({ where: { productId: id } }),
          tx.productRating.deleteMany({ where: { productId: id } }),
          tx.productHistory.deleteMany({ where: { productId: id } }),
          tx.product.delete({ where: { id: id } })
        ])

        return { id }
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )

    setTimeout(() => {
      removeImageFile(product.images)
    }, 0)

    return output
  }

  private async generateSlug(name: string) {
    let slug = toSlug(name)

    const count = await this._prisma.product.count({ where: { slug } })
    if (count > 0) {
      const count = await this._prisma.product.count()
      slug = `${slug}-${count + 1}`
    }

    return slug
  }

  private async generateSitemap() {
    const products = await this._prisma.product.findMany({
      select: { name: true, slug: true, createdAt: true, updatedAt: true },
      where: { isActive: true },
      orderBy: { updatedAt: 'desc' }
    })

    if (products.length === 0) {
      return
    }

    const lastmod = products[0].updatedAt || products[0].createdAt
    const indexItem = {
      //build index item
      url: [
        { loc: process.env.SITEMAP_DOMAIN },
        { lastmod: lastmod?.toISOString() },
        { changefreq: 'daily' },
        { priority: '1.0' }
      ]
    }

    const sitemapObject = {
      urlset: [
        {
          _attr: {
            xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
            ['xmlns:xsi']: 'http://www.w3.org/2001/XMLSchema-instance',
            ['xmlns:image']: 'http://www.google.com/schemas/sitemap-image/1.1',
            ['xsi:schemaLocation']:
              'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.google.com/schemas/sitemap-image/1.1 http://www.google.com/schemas/sitemap-image/1.1/sitemap-image.xsd'
          }
        },
        indexItem
      ]
    }

    products.forEach((p) => {
      sitemapObject.urlset.push({
        url: [
          { loc: `${process.env.SITEMAP_DOMAIN}/products/${p.slug}` },
          { lastmod: (p.updatedAt || p.createdAt)?.toISOString() }
        ]
      })
    })
    const sitemap = `<?xml version="1.0" encoding="UTF-8"?>${xml(sitemapObject)}`

    saveFile('sitemap', 'sitemap.xml', sitemap)
  }
}
