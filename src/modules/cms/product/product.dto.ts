import { IListQuery } from '@/modules/base/dto'
import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const CreateSchema = z.object({
  name: z.string().max(100),
  description: z.string().max(500).nullable().optional(),
  sku: z.string().nullable().optional(),
  price: z.number({ invalid_type_error: 'Price must be a number' }).nonnegative().nullable().optional(),
  categories: z.string().array().nullable().optional(),
  images: z.object({ thumbUrl: z.string(), name: z.string(), type: z.string() }).array().max(10).nullable().optional(),
  options: z.string().array().nullable().optional(),
  isActive: z.boolean().nullable().optional(),
  isFave: z.boolean().nullable().optional(),
  currencies: z
    .object({
      currency: z.string(),
      price: z.number({ invalid_type_error: 'Value must be a number' }).nonnegative(),
      saleOff: z.number({ invalid_type_error: 'Value must be a number' }).nonnegative().max(100),
      cost: z.number({ invalid_type_error: 'Value must be a number' }).nonnegative()
    })
    .array()
    .nullable()
    .optional()
})
export class CreateProductDto extends createZodDto(CreateSchema) {}

export class UpdateProductDto extends createZodDto(CreateSchema) {}

export interface IProductQuery extends IListQuery {
  option: string[]
  category: string[]
}
