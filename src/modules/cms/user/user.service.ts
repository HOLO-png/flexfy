import type { IFilter, IListQuery } from '@/modules/base/dto'
import { toCurrency } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'

@Injectable()
export class UserService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/user.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findMany(query: IListQuery) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, total] = await Promise.all([
      this._prisma.user.findMany({
        select: {
          id: true,
          lastname: true,
          firstname: true,
          email: true,
          phone: true,
          createdAt: true,
          confirmed: true,
          addresses: { select: { phone: true }, take: 1 }
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.user.count({ where })
    ])

    dataSource.forEach((d) => {
      d['key'] = d.id
    })

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const detail = await this._prisma.user.findFirst({
      where: { id },
      select: {
        id: true,
        lastname: true,
        firstname: true,
        email: true,
        createdAt: true,
        phone: true,
        addresses: {
          select: {
            phone: true,
            address: true,
            apartment: true,
            city: true,
            country: true,
            default: true,
            state: true,
            createdAt: true,
            zipCode: true
          }
        },
        orders: {
          select: {
            id: true,
            code: true,
            totalAmount: true,
            totalProduct: true,
            status: true,
            createdAt: true,
            paymentStatus: true,
            deliveryStatus: true,
            ordersProductsLinks: { select: { currency: true }, take: 1 }
          },
          orderBy: { createdAt: 'desc' }
        }
      }
    })
    if (!detail) {
      throw new NotFoundException()
    }

    detail.orders.forEach((o) => {
      const currency = o.ordersProductsLinks[0].currency
      o['currency'] = currency
      o.totalAmount = toCurrency(o.totalAmount) as any
      o.ordersProductsLinks = undefined as any
      o.code = { id: o.id, label: o.code, path: '/orders' } as any
      o['key'] = o.code
    })
    return detail
  }
}
