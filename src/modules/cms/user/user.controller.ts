import type { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Param, Query, UseInterceptors } from '@nestjs/common'
import { UserService } from './user.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('find')
  findMany(@Query() query: IListQuery) {
    return this.userService.findMany(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id)
  }
}
