import { isBefore } from '@/modules/base/util'
import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const DashboardQuerySchema = z.object({
  date: z
    .string()
    .array()
    .max(2)
    .nonempty()
    .refine((data) => isBefore(data[0], data[1]), { path: ['date'], message: 'values are not invalid' }),
  tz: z.string().nullable().optional()
})

export class DashboardQueryDto extends createZodDto(DashboardQuerySchema) {}
