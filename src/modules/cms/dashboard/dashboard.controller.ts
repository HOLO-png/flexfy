import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Query, UseInterceptors } from '@nestjs/common'
import { DashboardQueryDto } from './dashboard.dto'
import { DashboardService } from './dashboard.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) {}

  @Get('summary')
  findSummary(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findSummary(query)
  }

  @Get('cancel-reason')
  findCancelReason(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findCancelReason(query)
  }

  @Get('order-analytics')
  findOrderAnalytics(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findOrderAnalytics(query)
  }

  @Get('order-stats')
  findOrderStats(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findOrderStats(query)
  }

  @Get('top-products')
  findMany(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findTopProducts(query)
  }

  @Get('delivery-stats')
  findDeliveryStats(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findDeliveryStats(query)
  }

  @Get('aov')
  findAOV(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findAOV(query)
  }

  @Get('paid-stats')
  findPaidStats(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findPaidStats(query)
  }
}
