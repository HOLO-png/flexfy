import { BaseService } from '@/modules/base/service'
import { addDateToStr, dateToStr, diff, strToDate, toCurrency } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { DashboardQueryDto } from './dashboard.dto'

@Injectable()
export class DashboardService extends BaseService {
  async findPaidStats(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const [totalOrders, totalPaidOrder] = await Promise.all([
      this._prisma.order.count({ where }),
      this._prisma.order.aggregate({
        _count: { code: true },
        where: { AND: [{ paymentStatus: 'PAID' }, ...(where.AND || [])] }
      })
    ])

    const totalPaid = totalPaidOrder._count.code
    return { label: 'Payment Status', value: `${totalPaid} paid / ${totalOrders - totalPaid} unpaid` }
  }

  async findAOV(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.order.aggregate({
      _avg: { totalAmount: true },
      where
    })
    return { label: 'AOV', value: toCurrency(Math.floor(dataSource._avg.totalAmount || 0)) }
  }

  async findDeliveryStats(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.order.findMany({
      select: { deliveryStatus: true },
      where,
      orderBy: { createdAt: 'asc' }
    })

    const _map: Map<string | null, number> = new Map()
    dataSource.forEach((d) => {
      const key = d.deliveryStatus || 'Unknown'
      if (_map.has(key)) {
        const currentVal = _map.get(key) || 1
        _map.set(key, currentVal + 1)
      } else {
        _map.set(key, 1)
      }
    })

    const series: number[] = []
    const labels: (string | null)[] = []
    for (let [key, value] of _map.entries()) {
      labels.push(key)
      series.push(value)
    }

    return { series, labels }
  }

  async findOrderStats(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.order.findMany({ select: { status: true }, where })

    const _map: Map<string | null, number> = new Map()
    dataSource.forEach((d) => {
      if (_map.has(d.status)) {
        const currentVal = _map.get(d.status) || 1
        _map.set(d.status, currentVal + 1)
      } else {
        _map.set(d.status, 1)
      }
    })

    const series: number[] = []
    const labels: (string | null)[] = []
    for (let [key, value] of _map.entries()) {
      labels.push(key)
      series.push(value)
    }

    return { series, labels }
  }

  async findOrderAnalytics(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.order.findMany({
      select: {
        totalAmount: true, createdAt: true, paymentStatus: true,
      },
      where,
      orderBy: { createdAt: 'asc' }
    })

    const _map = this.withLabels(query)
    dataSource.forEach((d) => {
      const date = dateToStr(d.createdAt, this._tzOffset, query.tz);
      const [total, amount, revenue] = _map.get(date) || [1, 0, 0];
      const newTotal = total + 1;
      const newAmount = amount + (d.totalAmount || 0);
      const newRevenue = d.paymentStatus === 'PAID' ? revenue + (d.totalAmount || 0) : revenue;
      _map.set(date, [newTotal, newAmount, newRevenue]);
    });

    const totalOrders: number[] = []
    const totalAmount: number[] = []
    const totalRevenue: number[] = []
    const labels: (string | null)[] = []
    for (let [key, [total, amount, revenue]] of _map.entries()) {
      labels.push(key)
      totalOrders.push(total)
      totalAmount.push(amount)
      totalRevenue.push(revenue)
    }

    return {
      labels,
      series: [
        { name: 'Total Orders', type: 'column', data: totalOrders },
        { name: 'Total Amount', type: 'line', data: totalAmount },
        { name: 'Revenue', type: 'column', data: totalRevenue }
      ]
    }
  }

  async findSummary(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const [totalOrder, totalAmountRs, totalUser] = await Promise.all([
      this._prisma.order.count({ where }),
      this._prisma.order.aggregate({ where, _sum: { totalAmount: true } }),
      this._prisma.order.findMany({ where, select: { userId: true }, distinct: 'userId' })
    ])
    return {
      order: { label: 'Total Orders', value: totalOrder },
      amount: { label: 'Total Amount', value: toCurrency(totalAmountRs._sum.totalAmount || 0) },
      user: { label: 'Total Customers', value: totalUser.length }
    }
  }

  async findCancelReason(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    where.AND?.push({ status: 'CANCELLED' })
    const dataSource = await this._prisma.order.findMany({
      select: { cancelReason: true },
      where,
      orderBy: { createdAt: 'asc' }
    })

    const _map: Map<string | null, number> = new Map()
    dataSource.forEach((d) => {
      const key = d.cancelReason || 'Unknown'
      if (_map.has(key)) {
        const currentVal = _map.get(key) || 1
        _map.set(key, currentVal + 1)
      } else {
        _map.set(key, 1)
      }
    })

    const series: number[] = []
    const labels: (string | null)[] = []
    for (let [key, value] of _map.entries()) {
      labels.push(key)
      series.push(value)
    }

    return { series, labels }
  }

  async findTopProducts(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.orderProductLink.groupBy({
      by: ['productId'],
      where: {
        order: { AND: where.AND }
      },
      _count: { productId: true },
      orderBy: { _count: { productId: 'desc' } },
      take: 4
    })

    const pIds = dataSource.map((d) => d.productId || '')

    const products = await this._prisma.product.findMany({
      select: { id: true, name: true, slug: true, images: true },
      where: { id: { in: pIds } }
    })

    products.forEach((p, i) => {
      p['totalOrders'] = dataSource[i]._count.productId
    })
    return products
  }

  private withFilterDate(query: DashboardQueryDto): { AND?: Array<any> } {
    const where = {}

    const tz = query.tz || ''
    const AND: any[] = []
    const range = query['date'] as [string, string]
    AND.push({
      ['createdAt']: {
        gte: strToDate(range[0], this._tzOffset, 'start', tz),
        lte: strToDate(range[1], this._tzOffset, 'end', tz)
      }
    })

    where['AND'] = AND
    return where
  }

  private withLabels(query: DashboardQueryDto): Map<string | null, [number, number, number]> {
    const _map: Map<string | null, [number, number, number]> = new Map()
    const range = query['date'] as [string, string]
    const total = diff(range[1], 'day', range[0])
    _map.set(range[0], [0, 0, 0])
    for (let index = 1; index < total; index++) {
      _map.set(addDateToStr(range[0], this._tzOffset, index, 'day'), [0, 0, 0])
    }
    _map.set(range[1], [0, 0, 0])
    return _map
  }
}
