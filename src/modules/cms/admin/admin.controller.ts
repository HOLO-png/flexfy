import type { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseGuards, UsePipes } from '@nestjs/common'
import { readFileSync, readdirSync } from 'fs'
import { join } from 'path'
import { BaseController } from '../../base/controller'
import { CMSRoleGuard } from '../../guards/cms.guard'
import {
  CreateAdminRoleDto,
  CreateAdminUserDto,
  UpdateAdminRoleDto,
  UpdateAdminUserDto,
  UpdateUserProfileDto
} from './admin.dto'
import { AdminService } from './admin.service'

@UsePipes(CacheInterceptor)
@Controller('cms/admin')
export class AdminController extends BaseController {
  private readonly _schemas: Array<Record<string, any>> = []

  constructor(private readonly service: AdminService) {
    super()
    const schemaDir = readdirSync(join(process.cwd(), '/prisma/schemas'))
    schemaDir.forEach((file) => {
      const fileData = readFileSync(join(process.cwd(), '/prisma/schemas', file), 'utf-8')
      const schema = JSON.parse(fileData.toString())
      this._schemas.push(schema)
    })
  }

  @Get('schemas')
  getSchemas() {
    // TODO handle schema permission by user role
    return this._schemas
  }

  @Get('init')
  findUserMenu(@Req() req: Request) {
    return this.service.findUserMenu(req['user'])
  }

  @UseGuards(CMSRoleGuard)
  @Post('users')
  createAdminUser(@Body() body: CreateAdminUserDto, @Req() req: Request) {
    return this.service.createAdminUser(this.withUpdated(body, req))
  }

  @UseGuards(CMSRoleGuard)
  @Get('users/find')
  findAdminUsers(@Query() query: IListQuery) {
    return this.service.findAdminUsers(query)
  }

  @UseGuards(CMSRoleGuard)
  @Get('users/:id')
  findAdminUser(@Param('id') id: string) {
    return this.service.findAdminUser(id)
  }

  @Get('users/me')
  findUserProfile(@Req() req: Request) {
    return this.service.findAdminUser(req['user']['id'])
  }

  @Patch('users/me')
  updateUserProfile(@Body() body: UpdateUserProfileDto, @Req() req: Request) {
    return this.service.updateUserProfile(req['user']['id'], this.withUpdated(body, req))
  }

  @UseGuards(CMSRoleGuard)
  @Patch('users/:id')
  updateAdminUser(@Param('id') id: string, @Body() body: UpdateAdminUserDto, @Req() req: Request) {
    return this.service.updateAdminUser(id, this.withUpdated(body, req))
  }

  @UseGuards(CMSRoleGuard)
  @Post('roles')
  createAdminRole(@Body() body: CreateAdminRoleDto, @Req() req: Request) {
    return this.service.createAdminRole(this.withUpdated(body, req))
  }

  @Get('roles/relation')
  findAdminRelation() {
    return this.service.findAdminRelation()
  }

  @UseGuards(CMSRoleGuard)
  @Get('roles/find')
  findAdminRoles(@Query() query: IListQuery) {
    return this.service.findAdminRoles(query)
  }

  @UseGuards(CMSRoleGuard)
  @Get('roles/:id')
  findAdminRole(@Param('id') id: string) {
    return this.service.findAdminRole(id)
  }

  @UseGuards(CMSRoleGuard)
  @Patch('roles/:id')
  updateAdminRole(@Param('id') id: string, @Body() body: UpdateAdminRoleDto, @Req() req: Request) {
    return this.service.updateAdminRole(id, this.withUpdated(body, req))
  }

  @UseGuards(CMSRoleGuard)
  @Delete('roles/:id')
  removeAdminRole(@Param('id') id: string) {
    return this.service.removeAdminRole(id)
  }
}
