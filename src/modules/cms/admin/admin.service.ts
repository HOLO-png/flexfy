import { BaseDto, CurrentUser, IListDto, IListQuery } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { toId, toSlug } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { hashSync } from 'bcryptjs'
import {
  CreateAdminRoleDto,
  CreateAdminUserDto,
  UpdateAdminRoleDto,
  UpdateAdminUserDto,
  UpdateUserProfileDto
} from './admin.dto'

const menu = [
  { key: '/', label: 'Dashboard', path: '/' },
  { key: 'users', label: 'Users', path: '/users', viewable: true, creatable: true, exportable: true },
  { key: 'categories', label: 'Categories', path: '/categories', viewable: true, creatable: true, exportable: true },
  {
    key: 'product-options',
    label: 'Product Options',
    path: '/product-options',
    viewable: true,
    creatable: true,
    exportable: true
  },
  { key: 'products', label: 'Products', path: '/products', viewable: true, creatable: true, exportable: true },
  { key: 'product-rating', label: 'Product Rating', path: '/product-rating', viewable: true, creatable: false },
  {
    key: 'product-questions',
    label: 'Product Questions',
    path: '/product-questions',
    viewable: true,
    creatable: false
  },
  { key: 'orders', label: 'Orders', path: '/orders', viewable: true, creatable: true, exportable: true }
]

const STATIC_PAGES_MENU = [
  { key: 'static-pages/about-us', label: 'About Us', path: '/static-pages/about-us' },
  { key: 'static-pages/fit-guide', label: 'Fit Guide', path: '/static-pages/fit-guide' },
  { key: 'static-pages/size-charts', label: 'Size charts', path: '/static-pages/size-charts' },
  { key: 'static-pages/contact-us', label: 'Contact Us', path: '/static-pages/contact-us' },
  { key: 'static-pages/faq', label: 'FAQ', path: '/static-pages/faq' },
  { key: 'static-pages/privacy-terms', label: 'Privacy & Terms', path: '/static-pages/privacy-terms' },
  { key: 'static-pages/refund-policy', label: 'Refund Policy', path: '/static-pages/refund-policy' },
  { key: 'static-pages/shipping-policy', label: 'Shipping Policy', path: '/static-pages/shipping-policy' },
  { key: 'static-pages/privacy-policy', label: 'Privacy Policy', path: '/static-pages/privacy-policy' },
  { key: 'static-pages/terms-of-service', label: 'Terms Of Service', path: '/static-pages/terms-of-service' },
  { key: 'static-pages/contact-information', label: 'Contact Information', path: '/static-pages/contact-information' }
]

const SUPER_ADMIN_MENU = [
  { key: 'admin/users', label: 'Admin User', path: '/admin/users', viewable: true, creatable: true, exportable: true },
  { key: 'admin/roles', label: 'Admin Role', path: '/admin/roles', viewable: true, creatable: true, exportable: true }
]

const CONFIGURATION_MENU = [
  { key: 'static-pages/robots', label: 'Robot TXT', path: '/static-pages/robots', textarea: true },
  { key: 'static-pages/headers', label: 'Headers', path: '/static-pages/headers', textarea: true },
  { key: 'static-pages/ads-txt', label: 'Ads TXT', path: '/static-pages/ads-txt', textarea: true }
]

const FULL_PERMISSIONS = [
  { key: 'User', read: true, create: true, delete: true, update: true, collection: 'User' },
  { key: 'Category', read: true, create: true, delete: true, update: true, collection: 'Category' },
  { key: 'Product Option', read: true, create: true, delete: true, update: true, collection: 'Product Option' },
  { key: 'Product', read: true, create: true, delete: true, update: true, collection: 'Product' },
  { key: 'Order', read: true, create: true, delete: true, update: true, collection: 'Order' }
]

@Injectable()
export class AdminService extends BaseService {
  findUserMenu(user: CurrentUser) {
    // TODO check user permission
    if (user.roles.includes('super-admin')) {
      return menu.concat(SUPER_ADMIN_MENU).concat(STATIC_PAGES_MENU).concat(CONFIGURATION_MENU)
    }
    return menu.concat(STATIC_PAGES_MENU).concat(CONFIGURATION_MENU)
  }

  async createAdminUser(input: CreateAdminUserDto) {
    const { password, roles, ...data } = input
    const count = await this._prisma.adminUser.count({ where: { email: data.email } })
    if (count >= 1) {
      return this.withBadRequest(['email: Already used.'])
    } else {
      return this._prisma.adminUser.create({
        data: {
          id: toId(),
          password: hashSync(password),
          ...data,
          adminUserRoleLinks: {
            createMany: { data: roles.map((roleId) => ({ roleId, id: toId() })), skipDuplicates: true }
          }
        },
        select: { id: true }
      })
    }
  }

  async findAdminUsers(query: IListQuery): Promise<IListDto> {
    const { skip, take } = this.withLimitOffset(query)
    const [results, total] = await Promise.all([
      this._prisma.adminUser.findMany({
        select: {
          id: true,
          isActive: true,
          email: true,
          firstname: true,
          lastname: true,
          createdAt: true,
          adminUserRoleLinks: { select: { adminRole: { select: { name: true, code: true } } } }
        },
        where: {},
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.adminUser.count()
    ])

    const dataSource = results.map(({ adminUserRoleLinks, ...rest }) => {
      rest['key'] = rest.id
      rest['roles'] = adminUserRoleLinks.map((r) => r.adminRole?.name).join(', ')
      return rest
    })
    return this.withPagination(dataSource, query, total)
  }

  async findAdminUser(id: string) {
    const user = await this._prisma.adminUser.findFirst({
      where: { id },
      select: {
        id: true,
        email: true,
        firstname: true,
        lastname: true,
        isActive: true,
        adminUserRoleLinks: { select: { roleId: true } }
      }
    })

    if (!user) throw new NotFoundException()

    const { adminUserRoleLinks, ...output } = user
    output['roles'] = adminUserRoleLinks.map((x) => x.roleId)
    return output
  }

  async updateAdminUser(id: string, input: UpdateAdminUserDto) {
    const count = await this._prisma.adminUser.count({ where: { id } })
    if (count === 0) throw new NotFoundException()

    const { password, roles, ...data } = input
    if (password) {
      data['password'] = hashSync(password)
    }

    return this._prisma.$transaction(
      async (tx) => {
        if (roles && roles.length > 0) {
          const ids = await tx.adminUserRoleLink.findMany({ select: { id: true }, where: { userId: id } })
          await tx.adminUserRoleLink.deleteMany({ where: { id: { in: ids.map((i) => id) } } })
          await tx.adminUserRoleLink.createMany({
            skipDuplicates: true,
            data: roles.map((r) => ({ roleId: r, userId: id, id: toId() }))
          })
        }
        return await tx.adminUser.update({ where: { id }, data, select: { id: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  async updateUserProfile(id: string, input: UpdateUserProfileDto) {
    const count = await this._prisma.adminUser.count({ where: { id } })
    if (count === 0) throw new NotFoundException()

    const { password, ...data } = input
    if (password) {
      data['password'] = hashSync(password)
    }

    return this._prisma.adminUser.update({ where: { id }, data, select: { updatedAt: true } })
  }

  async createAdminRole(input: CreateAdminRoleDto & BaseDto) {
    const code = toSlug(input.name)
    const count = await this._prisma.adminRole.count({ where: { code } })
    if (count >= 1) {
      return this.withBadRequest(['name: Already used.'])
    } else {
      input.code = code
      input.id = toId()
      return this._prisma.adminRole.create({ data: input, select: { id: true } })
    }
  }

  async findAdminRoles(query: IListQuery): Promise<IListDto> {
    const { skip, take } = this.withLimitOffset(query)
    const [dataSource, total] = await Promise.all([
      this._prisma.adminRole.findMany({
        select: { id: true, name: true, code: true, description: true, createdAt: true },
        where: {},
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.adminRole.count()
    ])

    dataSource.forEach((x: any) => {
      x.key = x.id
      x.deletable = x.code !== 'super-admin'
    })
    return this.withPagination(dataSource, query, total)
  }

  async findAdminRelation() {
    const data = await this._prisma.adminRole.findMany({
      select: { id: true, name: true, code: true }
    })
    return data.map((d) => ({ value: d.id, label: d.name, disabled: d.code === 'super-admin' }))
  }

  async findAdminRole(id: string) {
    const role = await this._prisma.adminRole.findFirst({
      where: { id },
      select: { id: true, name: true, code: true, description: true, permissions: true }
    })

    if (!role) throw new NotFoundException()

    if (role.code === 'super-admin') {
      role.permissions = FULL_PERMISSIONS
    }
    return role
  }

  updateAdminRole(id: string, input: UpdateAdminRoleDto) {
    return this._prisma.adminRole.update({ where: { id }, data: input, select: { id: true } })
  }

  async removeAdminRole(id: string) {
    const count = await this._prisma.adminRole.count({ where: { id } })
    if (count === 0) throw new NotFoundException()

    return this._prisma.$transaction(
      async (tx) => {
        await tx.adminUserRoleLink.deleteMany({ where: { roleId: id } })
        return tx.adminRole.delete({ where: { id }, select: { id: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }
}
