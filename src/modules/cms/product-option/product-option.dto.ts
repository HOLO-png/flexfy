import { createZodDto } from '@anatine/zod-nestjs'
import { Option } from '@prisma/client'
import { z } from 'zod'

const CreateOptionSchema = z.object({
  name: z.string().max(20),
  code: z.string(),
  type: z.nativeEnum(Option),
  color: z.string().nullable().optional(),
  isActive: z.boolean().nullable().optional()
})
export class CreateProductOptionDto extends createZodDto(CreateOptionSchema) {}

export class UpdateProductOptionDto extends CreateProductOptionDto {}
