import { toId, toSlug } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { Option } from '@prisma/client'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { CreateProductOptionDto, UpdateProductOptionDto } from './product-option.dto'

import type { BaseDto, IFilter, IListQuery } from '@/modules/base/dto'

@Injectable()
export class ProductOptionService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/product-option.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async create(input: CreateProductOptionDto & BaseDto) {
    const { type, color, code, ...data } = input

    if (type === Option.COLOR) {
      if (!color) return this.withBadRequest(['color: Color is required'])
      data['color'] = color
    }
    data['code'] = await this.generateCode(data.name, code)
    data['type'] = type as Option
    data.id = toId()

    return this._prisma.productOption.create({ data, select: { id: true } })
  }

  async findMany(query: IListQuery) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)
    const [dataSource, total] = await Promise.all([
      this._prisma.productOption.findMany({
        select: {
          id: true,
          name: true,
          code: true,
          color: true,
          type: true,
          isActive: true,
          createdAt: true,
          createdBy: { select: { id: true, firstname: true, lastname: true } }
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.productOption.count({ where })
    ])
    dataSource.forEach((x: any) => {
      const createdBy = x.createdBy
      x.key = x.id
      // TODO check permission to remove
      x['deletable'] = true
      if (createdBy) {
        x['createdBy'] = {
          id: createdBy.id,
          label: `${createdBy.firstname || ''} ${createdBy.lastname || ''}`,
          path: '/users'
        } as any
      }
    })

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const detail = await this._prisma.productOption.findFirst({
      where: { id },
      select: {
        name: true,
        code: true,
        type: true,
        color: true,
        createdAt: true,
        isActive: true,
        productsOptionsLinks: {
          select: { product: { select: { id: true, name: true, imagesUpload: true, sku: true, isActive: true } } }
        }
      }
    })

    if (!detail) throw new NotFoundException()

    detail['products'] = detail.productsOptionsLinks.map((l) => {
      if (l.product) {
        const upload = l.product.imagesUpload as Array<{ thumbUrl: string }>
        l.product['key'] = l.product.id
        l.product['image'] = upload && upload.length > 0 ? upload[0]['thumbUrl'] : null
        l.product['name'] = { id: l.product.id, label: l.product.name, path: '/products' } as any
      }
      return l.product
    })
    detail.productsOptionsLinks = undefined as any

    return detail
  }

  async update(id: string, input: UpdateProductOptionDto) {
    const { type, color, code, ...data } = input

    if (type === Option.COLOR) {
      if (!color) return this.withBadRequest(['color: Color is required'])
      data['color'] = color
    }
    data['code'] = await this.generateCode(data.name, code)
    data['type'] = type

    return this._prisma.$transaction(
      async (tx) => {
        if (!data.isActive) {
          await tx.productOptionLink.deleteMany({ where: { optionId: id } })
        }

        return tx.productOption.update({ where: { id }, data, select: { updatedAt: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  async remove(id: string) {
    const result = await this._prisma.productOption.findUnique({ where: { id }, select: { id: true } })

    if (!result) {
      throw new NotFoundException()
    }
    return this._prisma.$transaction(
      async (tx) => {
        await tx.productOptionLink.deleteMany({ where: { optionId: id } })
        return tx.productOption.delete({ where: { id }, select: { id: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  async findRelation() {
    const results = await this._prisma.productOption.findMany({
      where: { isActive: true },
      select: { id: true, name: true, type: true }
    })

    return results.map((x) => ({ value: x.id, label: `${x.type} - ${x.name}` }))
  }

  private async generateCode(name: string, code?: string | null) {
    if (!code) {
      code = toSlug(name)
    }

    const count = await this._prisma.productOption.count({ where: { code } })
    if (count > 0) {
      const count = await this._prisma.productOption.count()
      code = `${code}-${count + 1}`
    }

    return code
  }
}
