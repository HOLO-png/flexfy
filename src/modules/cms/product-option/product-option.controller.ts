import { BaseController } from '@/modules/base/controller'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { CreateProductOptionDto, UpdateProductOptionDto } from './product-option.dto'
import { ProductOptionService } from './product-option.service'

import type { IListQuery } from '@/modules/base/dto'

@UseInterceptors(CacheInterceptor)
@Controller('cms/product-options')
export class ProductOptionController extends BaseController {
  constructor(private readonly service: ProductOptionService) {
    super()
  }

  @Post()
  create(@Body() body: CreateProductOptionDto, @Req() req: Request) {
    return this.service.create(this.withUpdated(body, req))
  }

  @Get('find')
  findMany(@Query() query: IListQuery) {
    return this.service.findMany(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateProductOptionDto, @Req() req: Request) {
    return this.service.update(id, this.withUpdated(body, req))
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id)
  }

  @Get('relation')
  findRelation() {
    return this.service.findRelation()
  }
}
