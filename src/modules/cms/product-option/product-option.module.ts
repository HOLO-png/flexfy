import { Module } from '@nestjs/common'
import { ProductOptionService } from './product-option.service'
import { ProductOptionController } from './product-option.controller'

@Module({
  controllers: [ProductOptionController],
  providers: [ProductOptionService]
})
export class ProductOptionModule {}
