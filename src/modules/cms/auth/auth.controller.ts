import { Body, Controller, Post, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { Public } from '../../guards/public.guard'
import { CredentialDto } from './auth.dto'
import { AuthService } from './auth.service'

@Controller('cms/auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private jwtService: JwtService) {}

  @Public()
  @Post('sign-in')
  async signIn(@Body() body: CredentialDto) {
    const user = await this.authService.checkCredentials(body.email.trim(), body.password)
    if (user) {
      const payload = { id: user.id, username: user.email, roles: user['roles'] }
      return {
        token: await this.jwtService.signAsync(payload, { secret: process.env.SECRET_TOKEN || 'SECRET_TOKEN' }),
        name: user.firstname + ' ' + user.lastname,
        roles: user['roles']
      }
    } else {
      throw new UnauthorizedException()
    }
  }
}
