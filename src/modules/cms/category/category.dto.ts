import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const CreateCategorySchema = z.object({
  name: z
    .string()
    .max(30)
    .regex(/^[\w\-\s&]+$/i, 'Value is not valid'),
  slug: z.string().nullable().optional(),
  isActive: z.boolean().nullable().optional(),
  isMain: z.boolean().nullable().optional(),
  subCategories: z.string().array().nullable().optional(),
  banner: z.object({ thumbUrl: z.string(), name: z.string(), type: z.string() }).array().max(3).nullable().optional(),
  description: z.string().max(500).nullable().optional()
})

export class CreateCategoryDto extends createZodDto(CreateCategorySchema) {}

export class UpdateCategoryDto extends CreateCategoryDto {}
