import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { CreateCategoryDto, UpdateCategoryDto } from './category.dto'
import { CategoryService } from './category.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/categories')
export class CategoryController extends BaseController {
  constructor(private readonly service: CategoryService) {
    super()
  }

  @Post()
  create(@Body() body: CreateCategoryDto, @Req() req: Request) {
    return this.service.create(this.withUpdated(body, req))
  }

  @Get('find')
  findMany(@Query() query: IListQuery, @Req() req: Request) {
    return this.service.findMany(query, req['user'])
  }

  @Get('relation')
  findRelation(@Query() query: { includeMain: string }) {
    return this.service.findRelation(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCategoryDto: UpdateCategoryDto, @Req() req: Request) {
    return this.service.update(id, this.withUpdated(updateCategoryDto, req))
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id)
  }
}
