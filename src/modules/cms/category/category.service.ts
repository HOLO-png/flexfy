import type { BaseDto, CurrentUser, IFilter, IListQuery } from '@/modules/base/dto'
import { removeImageFile, saveImageFile, toId, toSlug } from '@/modules/base/util'
import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { CreateCategoryDto, UpdateCategoryDto } from './category.dto'

@Injectable()
export class CategoryService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/category.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findMany(query: IListQuery, user: CurrentUser) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)
    const isSuperAdmin = user.roles.includes('super-admin')

    const [dataSource, total] = await Promise.all([
      this._prisma.category.findMany({
        select: {
          id: true,
          name: true,
          slug: true,
          isMain: true,
          isActive: true,
          parent: { select: { name: true }, where: { isActive: true } },
          createdAt: true,
          createdBy: { select: { id: true, firstname: true, lastname: true } }
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.category.count({ where })
    ])

    dataSource.forEach((x) => {
      const createdBy = x.createdBy
      x['key'] = x.id
      x['parent'] = x.parent ? x.parent.name : ('-' as any)
      x.isActive = x.isActive === null ? false : x.isActive
      x.isMain = x.isMain === null ? false : x.isMain
      x['deletable'] = isSuperAdmin || user.id === createdBy?.id
      if (createdBy) {
        x['createdBy'] = {
          id: createdBy.id,
          label: `${createdBy.firstname || ''} ${createdBy.lastname || ''}`,
          path: '/users'
        } as any
      }
      // TODO check user permission for delete
    })

    return this.withPagination(dataSource, query, total)
  }

  async findRelation(query: { includeMain: string }) {
    const where: Prisma.CategoryWhereInput = {}
    if (query.includeMain !== 'true') {
      where.OR = [{ isMain: null }, { isMain: { not: true } }]
    }
    where.isActive = true
    const results = await this._prisma.category.findMany({ where, select: { id: true, name: true } })
    return results.map((x) => ({ value: x.id, label: x.name }))
  }

  async findOne(id: string) {
    const result = await this._prisma.category.findUnique({
      where: { id },
      select: {
        name: true,
        slug: true,
        isActive: true,
        isMain: true,
        subCategories: { select: { id: true }, where: { isActive: true } },
        saleOff: true,
        description: true,
        bannerUpload: true
      }
    })
    if (!result) {
      throw new NotFoundException()
    }

    const subs = await this._prisma.category.findMany({ where: { parentId: id, isActive: true }, select: { id: true } })
    const cateIds = [id].concat(subs.map((s) => s.id))

    const products = await this._prisma.product.findMany({
      select: { id: true, name: true, sku: true, isActive: true },
      where: { categoriesLinks: { some: { categoryId: { in: cateIds } } } }
    })

    products.forEach((p) => {
      p['key'] = p.id
      p['name'] = { id: p.id, label: p.name, path: '/products' } as any
    })
    const { subCategories, bannerUpload, ...output } = result
    output['subCategories'] = subCategories.map((x) => x.id)
    output['banner'] = bannerUpload
    output['products'] = products
    return output
  }

  async create(input: CreateCategoryDto & BaseDto) {
    const { subCategories, banner: bannerUpload, slug, ...data } = input

    if (bannerUpload) {
      const images = await saveImageFile(bannerUpload, 'category')
      data['banner'] = images.join(', ')
      data['bannerUpload'] = bannerUpload
    }

    if (input.isMain && (!subCategories || Number(subCategories?.length) <= 0)) {
      throw new BadRequestException({
        message: ['subCategories: Please select sub-category!'],
        error: 'Bad Request',
        statusCode: 400
      })
    }
    return this._prisma.$transaction(
      async (tx) => {
        data['slug'] = await this.generateSlug(tx.category, data.name, slug)
        data['id'] = toId()
        const result = await tx.category.create({ data, select: { id: true } })
        if (input.isMain && subCategories && subCategories.length > 0) {
          await tx.category.updateMany({
            where: { id: { in: subCategories.map((x: string) => x) } },
            data: { parentId: result.id, updatedAt: data['updatedAt'], updatedById: data['updatedById'] }
          })
        }
        return result
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  async update(id: string, input: UpdateCategoryDto) {
    const { subCategories, banner: bannerUpload, slug, ...rest } = input

    const result = await this._prisma.category.findUnique({
      where: { id },
      select: { slug: true, bannerUpload: true, banner: true, subCategories: { select: { id: true } } }
    })

    if (!result) {
      throw new NotFoundException()
    }
    const data: Prisma.CategoryUncheckedUpdateInput = rest
    if (input.isMain && (!subCategories || Number(subCategories?.length) <= 0)) {
      throw new BadRequestException({
        message: ['subCategories: Please select sub-category!'],
        error: 'Bad Request',
        statusCode: 400
      })
    }
    if (bannerUpload) {
      // remove old and insert new
      removeImageFile(result.banner)

      const images = await saveImageFile(bannerUpload, 'category')
      data.banner = images.join(', ')
      data.bannerUpload = bannerUpload
    }

    return this._prisma.$transaction(
      async (tx) => {
        const bulkActions: any[] = []
        if (subCategories && subCategories.length > 0) {
          // remove old
          if (result.subCategories.length > 0) {
            bulkActions.push(
              tx.category.updateMany({
                where: { id: { in: result.subCategories.map((x) => x.id) } },
                data: { parentId: null, updatedAt: data['updatedAt'], updatedById: data['updatedById'] }
              })
            )
          }
          if (data.isMain) {
            // insert new
            bulkActions.push(
              tx.category.updateMany({
                where: { id: { in: subCategories.map((x) => x) } },
                data: { parentId: id, updatedAt: data['updatedAt'], updatedById: data['updatedById'] }
              })
            )
          }
        }

        if (!data.isActive || data.isMain) {
          if (!data.isActive) {
            data.parentId = null
          }
          bulkActions.push(tx.productCategoryLink.deleteMany({ where: { categoryId: id } }))
        }

        await Promise.all(bulkActions)
        return tx.category.update({ where: { id }, data: data, select: { updatedAt: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  remove(id: string) {
    return this._prisma.$transaction(
      async (tx) => {
        await tx.productCategoryLink.deleteMany({ where: { categoryId: id } })
        return tx.category.delete({ where: { id }, select: { id: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: this._transactionTimeOut, timeout: this._transactionTimeOut }
    )
  }

  private async generateSlug(category: Prisma.CategoryDelegate, name: string, slug?: string | null) {
    if (!slug) {
      slug = toSlug(name)
    }

    const count = await category.count({ where: { slug } })
    if (count > 0) {
      const count = await category.count()
      slug = `${slug}-${count + 1}`
    }

    return slug
  }
}
