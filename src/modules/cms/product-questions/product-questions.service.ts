import type { IFilter, IListQuery } from '@/modules/base/dto'
import { Injectable, NotFoundException } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { QuestionRepliedDto } from './product-questions.dto'

@Injectable()
export class ProductQuestionsService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/product-question.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async reply(id: string, input: QuestionRepliedDto) {
    const exist = await this._prisma.productQuestion.count({ where: { id } })
    if (!exist) {
      throw new NotFoundException()
    }
    const data: Prisma.ProductQuestionUncheckedUpdateInput = {
      replied: input.replied,
      repliedById: input['updatedById'],
      repliedAt: input['updatedAt']
    }

    return this._prisma.productQuestion.update({ data, where: { id }, select: { repliedAt: true } })
  }

  async findQuestions(query: IListQuery) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)
    const [dataSource, total] = await Promise.all([
      this._prisma.productQuestion.findMany({
        select: {
          id: true,
          author: true,
          email: true,
          question: true,
          createdAt: true,
          replied: true,
          repliedAt: true,
          repliedBy: { select: { id: true, firstname: true, lastname: true } },
          product: { select: { id: true, name: true } }
        },
        where,
        take,
        skip,
        orderBy: { createdAt: 'desc' }
      }),
      this._prisma.productQuestion.count({ where })
    ])

    dataSource.forEach((x) => {
      x['key'] = x.id
      x['deletable'] = true
      x['product'] = { id: x.product.id, label: x.product.name, path: '/products' } as any
      x['repliedBy'] = x.repliedBy
        ? ({
            id: x.repliedBy.id,
            label: `${x.repliedBy.firstname || ''} ${x.repliedBy.lastname || ''}`,
            path: '/admin/users'
          } as any)
        : null
    })

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const question = await this._prisma.productQuestion.findUnique({
      select: {
        id: true,
        author: true,
        email: true,
        question: true,
        replied: true,
        createdAt: true,
        repliedAt: true,
        repliedBy: { select: { firstname: true, lastname: true } }
      },
      where: { id }
    })

    if (!question) {
      throw new NotFoundException()
    }

    question.repliedBy = question.repliedBy
      ? (`${question.repliedBy.firstname} ${question.repliedBy.lastname}` as any)
      : null
    return question
  }

  async remove(id: string) {
    const exist = await this._prisma.productQuestion.count({ where: { id } })
    if (!exist) {
      throw new NotFoundException()
    }

    return this._prisma.productQuestion.delete({ where: { id }, select: { id: true } })
  }
}
