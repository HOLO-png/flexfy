import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Query, Req, UseInterceptors } from '@nestjs/common'
import { QuestionRepliedDto } from './product-questions.dto'
import { ProductQuestionsService } from './product-questions.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/product-questions')
export class ProductQuestionsController extends BaseController {
  constructor(private readonly service: ProductQuestionsService) {
    super()
  }

  @Patch(':id')
  reply(@Param('id') id: string, @Body() body: QuestionRepliedDto, @Req() req: Request) {
    return this.service.reply(id, this.withUpdated(body, req))
  }

  @Get('find')
  async findQuestions(@Query() query: IListQuery) {
    return this.service.findQuestions(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(id)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id)
  }
}
