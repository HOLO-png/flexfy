import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const QuestionRepliedSchema = z.object({ replied: z.string().max(512) })
export class QuestionRepliedDto extends createZodDto(QuestionRepliedSchema) {}
