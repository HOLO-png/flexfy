import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Query, Req, UseInterceptors } from '@nestjs/common'
import { ApproveRatingDto } from './product-rating.dto'
import { ProductRatingService } from './product-rating.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/product-rating')
export class ProductRatingController extends BaseController {
  constructor(private readonly service: ProductRatingService) {
    super()
  }

  @Patch(':id')
  approve(@Param('id') id: string, @Body() body: ApproveRatingDto, @Req() req: Request) {
    return this.service.approve(id, this.withUpdated(body, req))
  }

  @Get('find')
  async findRating(@Query() query: IListQuery) {
    return this.service.findRating(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(id)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id)
  }
}
