import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

export enum ApproveEnum {
  YES = 'YES',
  NO = 'NO'
}
const ApproveSchema = z.object({ isApproved: z.nativeEnum(ApproveEnum) })
export class ApproveRatingDto extends createZodDto(ApproveSchema) {}
