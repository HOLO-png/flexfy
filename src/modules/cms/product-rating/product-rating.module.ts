import { Module } from '@nestjs/common'
import { ProductRatingController } from './product-rating.controller'
import { ProductRatingService } from './product-rating.service'

@Module({
  controllers: [ProductRatingController],
  providers: [ProductRatingService]
})
export class ProductRatingModule {}
