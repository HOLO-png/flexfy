import type { IFilter, IListQuery } from '@/modules/base/dto'
import { Injectable, NotFoundException } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { ApproveEnum, ApproveRatingDto } from './product-rating.dto'

@Injectable()
export class ProductRatingService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/product-rating.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findRating(query: IListQuery) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, total] = await Promise.all([
      this._prisma.productRating.findMany({
        select: {
          id: true,
          author: true,
          email: true,
          title: true,
          comment: true,
          createdAt: true,
          approvedAt: true,
          approvedBy: { select: { id: true, firstname: true, lastname: true } },
          product: { select: { id: true, name: true } }
        },
        where,
        take,
        skip,
        orderBy: { createdAt: 'desc' }
      }),
      this._prisma.productRating.count({ where })
    ])

    dataSource.forEach((x) => {
      x['key'] = x.id
      x['deletable'] = true
      x['isApproved'] = x.approvedBy !== null
      x['product'] = { id: x.product.id, label: x.product.name, path: '/products' } as any
      x['approvedBy'] = x.approvedBy
        ? ({
            id: x.approvedBy.id,
            label: `${x.approvedBy.firstname || ''} ${x.approvedBy.lastname || ''}`,
            path: '/admin/users'
          } as any)
        : null
    })

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const rating = await this._prisma.productRating.findUnique({
      select: {
        id: true,
        author: true,
        email: true,
        title: true,
        comment: true,
        createdAt: true,
        approvedAt: true,
        approvedBy: { select: { firstname: true, lastname: true } }
      },
      where: { id }
    })

    if (!rating) {
      throw new NotFoundException()
    }

    rating['isApproved'] = rating.approvedBy ? ApproveEnum.YES : ApproveEnum.NO
    rating.approvedBy = rating.approvedBy
      ? (`${rating.approvedBy.firstname} ${rating.approvedBy.lastname}` as any)
      : null
    return rating
  }

  async approve(id: string, input: ApproveRatingDto) {
    const exist = await this._prisma.productRating.count({ where: { id } })
    if (!exist) {
      throw new NotFoundException()
    }

    const data: Prisma.ProductRatingUncheckedUpdateInput = { approvedAt: null, approvedById: null }
    if (ApproveEnum.YES === input.isApproved) {
      data.approvedAt = input['updatedAt']
      data.approvedById = input['updatedById']
    }

    const rating = await this._prisma.productRating.update({
      where: { id },
      data,
      select: { approvedAt: true, approvedBy: { select: { firstname: true, lastname: true } } }
    })

    rating.approvedBy = rating.approvedBy
      ? (`${rating.approvedBy.firstname} ${rating.approvedBy.lastname}` as any)
      : null
    return rating
  }

  async remove(id: string) {
    const exist = await this._prisma.productRating.count({ where: { id } })
    if (!exist) {
      throw new NotFoundException()
    }

    return this._prisma.productRating.delete({ where: { id }, select: { id: true } })
  }
}
