import { Public } from '@/modules/guards/public.guard'
import { Controller, Get } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'

@Controller('thunder')
export class ThunderController {
  @Public()
  @Get()
  getThunder() {
    return readFileSync(join(process.cwd(), 'thunder/thunder-collection_flexfy-backend.json'), { encoding: 'utf-8' })
  }

  @Public()
  @Get('/landing')
  getThunderLanding() {
    const data = JSON.parse(
      readFileSync(join(process.cwd(), 'thunder/thunder-collection_flexfy-backend.json'), { encoding: 'utf-8' })
    ).requests.filter((req) => req.url.includes('/landing')) as any[]

    return data.map((d) => ({ _id: d._id, url: d.url, method: d.method }))
  }
}
