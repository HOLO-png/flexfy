import dayjs from 'dayjs'
import 'dotenv/config'

import { NestFactory } from '@nestjs/core'
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import { AppModule } from './app.module'

BigInt.prototype['toJSON'] = function () {
  return this.toString()
}
const startApp = async () => {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ caseSensitive: true, bodyLimit: 50 * 1024 * 2024 })
  )

  app.enableCors()
  await app.listen(33333, '0.0.0.0')
}

async function bootstrap() {
  await startApp()
}
bootstrap()

export const dayjsImport = dayjs
